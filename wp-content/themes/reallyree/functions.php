<?php

/*-----------------------------------------------------------------------------------*/
/* Start WooThemes Functions - Please refrain from editing this section */
/*-----------------------------------------------------------------------------------*/

// Set path to WooFramework and theme specific functions
$functions_path = get_template_directory() . '/functions/';
$includes_path = get_template_directory() . '/includes/';

// Don't load alt stylesheet from WooFramework
if ( ! function_exists( 'woo_output_alt_stylesheet' ) ) {
	function woo_output_alt_stylesheet () {}
}

// Define the theme-specific key to be sent to PressTrends.
define( 'WOO_PRESSTRENDS_THEMEKEY', 'tnla49pj66y028vef95h2oqhkir0tf3jr' );

// WooFramework
require_once ( $functions_path . 'admin-init.php' );	// Framework Init

if ( get_option( 'woo_woo_tumblog_switch' ) == 'true' ) {
	//Enable Tumblog Functionality and theme is upgraded
	update_option( 'woo_needs_tumblog_upgrade', 'false' );
	update_option( 'tumblog_woo_tumblog_upgraded', 'true' );
	update_option( 'tumblog_woo_tumblog_upgraded_posts_done', 'true' );
	require_once ( $functions_path . 'admin-tumblog-quickpress.php' );  // Tumblog Dashboard Functionality
}

/*-----------------------------------------------------------------------------------*/
/* Load the theme-specific files, with support for overriding via a child theme.
/*-----------------------------------------------------------------------------------*/

$includes = array(
				'includes/theme-options.php', 				// Options panel settings and custom settings
				'includes/theme-functions.php', 			// Custom theme functions
				'includes/theme-actions.php', 				// Theme actions & user defined hooks
				'includes/theme-comments.php', 				// Custom comments/pingback loop
				'includes/theme-js.php', 					// Load JavaScript via wp_enqueue_script
				'includes/theme-plugin-integrations.php',	// Plugin integrations
				'includes/sidebar-init.php', 				// Initialize widgetized areas
				'includes/theme-widgets.php',				// Theme widgets
				'includes/theme-advanced.php',				// Advanced Theme Functions
				'includes/theme-shortcodes.php',	 		// Custom theme shortcodes
				'includes/woo-layout/woo-layout.php',		// Layout Manager
				'includes/woo-meta/woo-meta.php',			// Meta Manager
				'includes/woo-hooks/woo-hooks.php'			// Hook Manager
				);

// Allow child themes/plugins to add widgets to be loaded.
$includes = apply_filters( 'woo_includes', $includes );

foreach ( $includes as $i ) {
	locate_template( $i, true );
}

// Load WooCommerce functions, if applicable.
if ( is_woocommerce_activated() ) {
	locate_template( 'includes/theme-woocommerce.php', true );
}

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/

	/* Add theme support for post thumbnails (featured images) */
	if ( function_exists( 'add_theme_support' ) ) {
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size(320, '212', true); // sets width and height for thumbnail
		add_image_size( 'home-left', 630, 415, true);
		add_image_size( 'blog', 292, 194, true );
	}
	

function woo_archive_title ( $before = '', $after = '', $echo = true ) {

    global $wp_query;

    if ( is_category() || is_tag() || is_tax() ) {

        $taxonomy_obj = $wp_query->get_queried_object();
        $term_id = $taxonomy_obj->term_id;
        $taxonomy_short_name = $taxonomy_obj->taxonomy;

        $taxonomy_raw_obj = get_taxonomy( $taxonomy_short_name );

    } // End IF Statement

    $title = '';
    $delimiter = ' | ';
    $date_format = get_option( 'date_format' );

    // Category Archive
    if ( is_category() ) {

        $title = '<span class="fl cat">' . single_cat_title( '', false ) . '</span> <span class="fr catrss">';
        $cat_obj = $wp_query->get_queried_object();
        $cat_id = $cat_obj->cat_ID;
        $title .= '<a href="' . get_term_feed_link( $term_id, $taxonomy_short_name, '' ) . '">' . __( 'RSS feed for this section','woo themes' ) . '</a></span>';

        $has_title = true;
    }

    // Day Archive
    if ( is_day() ) {

        $title = __( 'Archive', 'woo themes' ) . $delimiter . get_the_time( $date_format );
    }

    // Month Archive
    if ( is_month() ) {

        $date_format = apply_filters( 'woo_archive_title_date_format', 'F, Y' );
        $title = __( 'Archive', 'woo themes' ) . $delimiter . get_the_time( $date_format );
    }

    // Year Archive
    if ( is_year() ) {

        $date_format = apply_filters( 'woo_archive_title_date_format', 'Y' );
        $title = __( 'Archive', 'woo themes' ) . $delimiter . get_the_time( $date_format );
    }

    // Author Archive
    if ( is_author() ) {

        $title = __( 'Author Archive', 'woo themes' ) . $delimiter . get_the_author_meta( 'display_name', get_query_var( 'author' ) );
    }

    // Tag Archive
    if ( is_tag() ) {

        $title = __( 'Tag Archives', 'woo themes' ) . $delimiter . single_tag_title( '', false );
    }

    // Post Type Archive
    if ( function_exists( 'is_post_type_archive' ) && is_post_type_archive() ) {

        /* Get the post type object. */
        $post_type_object = get_post_type_object( get_query_var( 'post_type' ) );

        $title = $post_type_object->labels->name . ' ' . __( 'Archive', 'woo themes' );
    }

    // Post Format Archive
    if ( get_query_var( 'taxonomy' ) == 'post_format' ) {

        $post_format = str_replace( 'post-format-', '', get_query_var( 'post_format' ) );

        $title = get_post_format_string( $post_format ) . ' ' . __( ' Archives', 'woo themes' );
    }

    // General Taxonomy Archive
    if ( is_tax() ) {

        $title = sprintf( __( '%1$s Archives: %2$s', 'woothemes' ), $taxonomy_raw_obj->labels->name, $taxonomy_obj->name );

    }

    if ( strlen($title) == 0 )
    return;

    $title = $before . $title . $after;

    // Allow for external filters to manipulate the title value.
    $title = apply_filters( 'woo_archive_title', $title, $before, $after );

    if ( $echo )
        echo $title;
    else
        return $title;

} // End woo_archive_title()

/*-----------  Remove unnecessary meta-data    ------------------------------*/
remove_action( 'wp_head', 'wp_generator' ) ;
remove_action( 'wp_head', 'wlwmanifest_link' ) ;
remove_action( 'wp_head', 'rsd_link' ) ;
/*-----------  End Remove unnecessary meta-data    ---------------------------*/


/*------------- Hide the secondary WordPress feeds ------------------------------*/
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'feed_links_extra', 3 );
/*-----------  End Hide the secondary WordPress feeds ------------------------------*/


/*------------- Stop WordPress from Guessing URLs ------------------------------*/
add_filter('redirect_canonical', 'stop_guessing');
function stop_guessing($url) {  if (is_404()) {    return false;  }  return $url; }

/*--------- End Stop WordPress from Guessing URLs ------------------------------*/

/*--------- Disable File Editing inside WordPress  ------------------------------*/
define( 'DISALLOW_FILE_EDIT', true );
/*------- end  Disable File Editing inside WordPress  ------------------------------*/



@ini_set( 'upload_max_size' , '50M' );

/*------------------ Blogger 301 redirect ------------------------------*/

function labnol_blogger_query_vars_filter( $vars ) {
    $vars[] = "blogger";
    return $vars;
    }

    add_filter('query_vars', 'labnol_blogger_query_vars_filter');

    function labnol_blogger_template_redirect() {
    global $wp_query;
    $blogger = $wp_query->query_vars['blogger'];
    if ( isset ( $blogger ) ) {
    wp_redirect( labnol_get_wordpress_url ( $blogger ) , 301 );
    exit;
    }
    }

    add_action( 'template_redirect', 'labnol_blogger_template_redirect' );

    function labnol_get_wordpress_url($blogger_slug) {
    global $wpdb;
    if ( preg_match('@^(?:https?://)?([^/]+)(.*)@i', $blogger_slug, $matches) ) {
    $q = "SELECT guid FROM $wpdb->posts LEFT JOIN $wpdb->postmeta
    ON ($wpdb->posts.ID = $wpdb->postmeta.post_id)
    WHERE $wpdb->postmeta.meta_key='blogger_permalink'
    AND $wpdb->postmeta.meta_value='$matches[2]'";
    $wp_url = $wpdb->get_var($q);
    }
    return $wp_url ? $wp_url : home_url();
    }

/*-------------------- End Blogger 301 redirect ------------------------------*

/*-------------------- change excerpt length ---------------------------------*/

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/*---------------- end change excerpt length ---------------------------------*/


/**
 * A custom shortcode to display top rated posts.
 *
*/
function elm_custom_top_rated( $atts ) {
 
    $atts = shortcode_atts( array(
        'post_type' => '',
        'sort' => 'asc',
        'limit' => ''
    ), $atts );
           
    global $wpdb;
 
    // LIMIT posts
    $atts['limit'] = 20;
           
    if ( !$atts['post_type'] ) {
        $sql = "SELECT DISTINCT(post_id) FROM {$wpdb->prefix}elm_ratings";
               
        if ( $atts['limit'] )
            $sql .= " LIMIT {$atts['limit']}";
               
            $get_posts = $wpdb->get_results( $sql );
    } else {
        $sql = "SELECT DISTINCT(post_id) FROM {$wpdb->prefix}elm_ratings WHERE type = '{$atts['post_type']}'";
           
        if ( $atts['limit'] )
            $sql = "SELECT DISTINCT(post_id) FROM {$wpdb->prefix}elm_ratings WHERE type = '{$atts['post_type']}' LIMIT {$atts['limit']}";
           
        $get_posts = $wpdb->get_results( $sql );
    }
       
        if ( empty( $get_posts ) )
            return;
       
        foreach ( $get_posts as $k => $post ) {
            $average = intval( get_post_meta( $post->post_id, '_average_page_rating', TRUE ) );
           
            $posts[$k]['id']             = $post->post_id;
            $posts[$k]['average_rating'] = $average;
        }
       
        foreach ( $posts as $k => $v ) {
            $b[$k] = intval( $v['average_rating'] );
        }
       
        // Sort
        if ( $atts['sort'] == 'asc' ) {
            arsort( $b );
        } else {
            asort( $b );
        }
        
        $html = '<ul class="reallyrated">';
        
        // HTML output
        if ( $b ) {            
            foreach ( $b as $key => $val ) {
				$html .= '<li class="post postgrid">';
				
				if ( has_post_thumbnail($posts[$key]['id']) ) {
					$html .= '<a href="'. get_permalink( $posts[$key]['id'] ) .'">';
				}
				$html .= '<h3 class="onpost">'. get_the_title( $posts[$key]['id'] ) .'</h3>' . get_the_post_thumbnail( $posts[$key]['id'] ) . '</a>';
				$html .= '</li>';
            }
        }
        
    return $html;
 }

add_shortcode( 'elm_custom_top_rated', 'elm_custom_top_rated' );


/*-------------------- remove shortcode from excerpt ----------------------------*/
function remove_shortcode_from_excerpt($content) {
  $content = strip_shortcodes( $content );
  return $content;//always return $content
  }
add_filter('the_excerpt', 'remove_shortcode_from_excerpt');

/*---------------- end remove shortcode from excerpt ----------------------------*/


/*---------------- offset first 6 posts from daily beauty category----------------*/

add_action('pre_get_posts', 'myprefix_query_offset', 1 );
function myprefix_query_offset(&$query) {

    //Before anything else, make sure this is the right query...
    if ( ! $query->is_category('daily-beauty') ) {
        return;
    }

    //First, define your desired offset...
    $offset = 6;
    
    //Next, determine how many posts per page you want (we'll use WordPress's settings)
    $ppp = get_option('posts_per_page');

    //Next, detect and handle pagination...
    if ( $query->is_paged ) {

        //Manually determine page query offset (offset + current page (minus one) x posts per page)
        $page_offset = $offset + ( ($query->query_vars['paged']-1) * $ppp );

        //Apply adjust page offset
        $query->set('offset', $page_offset );

    }
    else {

        //This is the first page. Just use the offset...
        $query->set('offset',$offset);

    }
}

/*------------ end offset first 6 posts from daily beauty category----------------*/


/*------------ update draft date to publish ----------------*/

    function my_update_published_date( $post )   
    {  
        // I only wanted this to apply for a particular post type  
        if ($post->post_type == 'myposttype')   
        {  
            // Update the post  
            $my_post = array(  
                'ID'            => $post->ID,  
                'post_date'     => date("Y-m-d H:i:s"),  
                'post_date_gmt'     => date("Y-m-d H:i:s")  
            );  
          
            // Update the post into the database  
            wp_update_post( $my_post );  
        }  
    }  
    add_action( 'draft_to_publish', 'my_update_published_date' );  

/*------------ end update draft date to publish ----------------*/

/*------------ custom post types ----------------*/

// Our custom post type function
function create_posttype() {

		register_post_type( 'beauty-offers',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Beauty Offers' ),
				'singular_name' => __( 'Beauty Offer' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'beauty-offers'),
		)
	);
	
		register_post_type( 'contributors',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Contributors' ),
				'singular_name' => __( 'Contributor' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'contributors'),
		)
	);
	
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


/*
* Creating a function to create our CPT
*/

function custom_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Beauty Offers', 'Post Type General Name', 'twentythirteen' ),
		'singular_name'       => _x( 'Beauty Offer', 'Post Type Singular Name', 'twentythirteen' ),
		'menu_name'           => __( 'Beauty Offers', 'twentythirteen' ),
		'parent_item_colon'   => __( 'Parent Beauty Offer', 'twentythirteen' ),
		'all_items'           => __( 'All Beauty Offers', 'twentythirteen' ),
		'view_item'           => __( 'View Beauty Offer', 'twentythirteen' ),
		'add_new_item'        => __( 'Add New Beauty Offer', 'twentythirteen' ),
		'add_new'             => __( 'Add New', 'twentythirteen' ),
		'edit_item'           => __( 'Edit Beauty Offer', 'twentythirteen' ),
		'update_item'         => __( 'Update Beauty Offer', 'twentythirteen' ),
		'search_items'        => __( 'Search Beauty Offers', 'twentythirteen' ),
		'not_found'           => __( 'Not Found', 'twentythirteen' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'beauty-offers', 'twentythirteen' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
	// Registering your Custom Post Type
	register_post_type( 'beauty-offers', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'custom_post_type', 0 );


/*-------- end custom post types ----------------*/

/*-------- beauty offers post per page ----------------*/

function wpd_beautyoffers_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'beauty-offers' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', 100 );
    }
}
add_action( 'pre_get_posts', 'wpd_beautyoffers_query' );

/*-------- end beauty offers post per page ----------------*/

/*--------------------- stripes  ----------------*/

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it topics for your posts

function create_topics_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

  $labels = array(
    'name' => _x( 'Stripes', 'taxonomy general name' ),
    'singular_name' => _x( 'Stripe', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Stripes' ),
    'all_items' => __( 'All Stripes' ),
    'parent_item' => __( 'Parent Stripe' ),
    'parent_item_colon' => __( 'Parent Stripe:' ),
    'edit_item' => __( 'Edit Stripe' ), 
    'update_item' => __( 'Update Custom Category' ),
    'add_new_item' => __( 'Add New Stripe' ),
    'new_item_name' => __( 'New Stripe Name' ),
    'menu_name' => __( 'Stripes' ),
  ); 	

// Now register the taxonomy

  register_taxonomy('stripes',array('post'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'stripe' ),
  ));

}

/*------------- end stripes  ----------------*/

/*-----------------------------------------------------------------------------------*/
/* Don't add any code below here or the sky will fall down */
/*-----------------------------------------------------------------------------------*/



?>
