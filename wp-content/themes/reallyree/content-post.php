<?php
/**
 * Post Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */

$settings = array(
				'thumb_w' => 100,
				'thumb_h' => 100,
				'thumb_align' => 'alignleft',
				'post_content' => 'excerpt',
				'comments' => 'both'
				);

$settings = woo_get_dynamic_values( $settings );

$title_before = '<h1 class="title entry-title">';
$title_after = '</h1>';

if ( ! is_single() ) {
$title_before = '<h2 class="title entry-title">';
$title_after = '</h2>';
$title_before = $title_before . '<a href="' . esc_url( get_permalink( get_the_ID() ) ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';
$title_after = '</a>' . $title_after;
}

$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

woo_post_before();
?>

<article <?php post_class(); ?>>
<div class="stripe"><?php the_terms( $post->ID, 'stripes' ); ?></div>
	<?php
woo_post_inside_before();
if ( 'content' != $settings['post_content'] && ! is_singular() )
	woo_image( 'width=' . esc_attr( $settings['thumb_w'] ) . '&height=' . esc_attr( $settings['thumb_h'] ) . '&class=thumbnail ' . esc_attr( $settings['thumb_align'] ) );
?>

<header>

<!-- don't miss out rondel -->

<?php if( get_field('dont_miss_out') == 'yes' ): ?>


	<div class="dontmissout">
		<img src="http://images.reallyree.com/wp-content/themes/reallyree/images/dontmissout.png" />
	</div>


<?php else : ?>

<?php endif; ?>


	
<!-- end don't miss out rondel -->
	
	<?php the_title( $title_before, $title_after ); ?>
	<?php if( get_field('subhead') ): ?>
	<div class="subhead">
  		<?php the_field('subhead'); ?>
  </div>
	<?php endif; ?>
	</header>

<?php
if ( 'post' == get_post_type() && is_single()) {
    do_action( 'if_affiliator_public');
}
?>	
	
	<section class="entry">
	

<?php if(is_single()) : ?>
<div class="postmeta">
  <span class="postauthor">BY <?php the_author_posts_link(); ?>,</span>
<hr />
</div>
  

	<?php if( get_field('sponsored_by') ): ?>
	<div class="sponsoredby">
	 <?php if( get_field('weblink') ): ?>
	 <a href="http://<?php the_field('weblink'); ?>" target="_blank">
	 <?php endif; ?>
		<img class="sponsored" src="<?php the_field('sponsored_by'); ?>" />
		<?php if( get_field('weblink') ): ?></a><?php endif; ?>
		<div class="featuredpartner">FEATURED PARTNER</div>
	</div>
	<?php endif; ?>
	
<?php endif; ?>

<div class="contributedby">
  <span class="postauthor"><?php the_author_posts_link(); ?></span>
</div>




<?php
if ( 'content' == $settings['post_content'] || is_single() ) { the_content( __( 'Continue Reading &rarr;', 'woothemes' ) ); } else { the_excerpt(); }

if ( 'content' == $settings['post_content'] || is_singular() ) wp_link_pages( $page_link_args );
?>
<span class="viewmore" id="inpost"><a href="<?php the_permalink() ?>">VIEW MORE ></a></span>

<?php
  if ( 'post' == get_post_type() && is_single()) {
   do_action( 'if_affiliator_subscribe_form_public');
  }
 ?>
 
<?php if(is_single()) : ?>
	<p class="disclaimer">Unless otherwise indicated products reviewed are press or brand samples. Links may be affiliate links which means that if you make a purchase through one of our links we receive a small commission which helps support and run this website.</p>
	<div class="facebookconversation">
	<h3>Facebook Conversations</h3>
	<hr / class="facebookhr">
  <div class="fb-comments" data-href="<?php the_permalink() ?>" data-numposts="100" data-colorscheme="light" width="100%"> </div>
  </div>
  
  <?php
woo_post_after();
$comm = $settings['comments'];
if ( ( 'post' == $comm || 'both' == $comm ) && is_single() ) { comments_template(); }
?>
  
<div id="singlerecents">
<h3 id="morefrom"><a href="<?php bloginfo('home'); ?>/category/daily-beauty">MORE FROM DAILY BEAUTY ></a></h3>
<?php
echo do_shortcode('[do_widget id=text-36]');
?> 
  <div class="clear"></div>
  
  
  
<?php  global $post;
$current_post = $post; // remember the current post

for($i = 1; $i <= 3; $i++){
  $post = get_previous_post(); // this uses $post->ID
  setup_postdata($post);
?>
<div class="singlerecentpost">
	<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('blog'); ?></a>
	<h2 class="title entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
	<?php echo strip_tags( substr( $post->post_content, 0, 120 ) ); ?>...
	<span class="viewmore"><a href="<?php the_permalink() ?>">VIEW MORE ></a></span>
	<div class="clear"></div>
	<hr />
<?php
  // do your stuff here 

}
?>
<?php
$post = $current_post; // restore
 ?> 
 
 </div>
 
<?php endif; ?> 


	</section><!-- /.entry -->



	
	<div class="fix"></div>
<div class="hidesingle">
  <hr />
</div>
</article><!-- /.post -->