<?php
/**
 * Post Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */

$settings = array(
				'thumb_w' => 100,
				'thumb_h' => 100,
				'thumb_align' => 'alignleft',
				'post_content' => 'excerpt',
				'comments' => 'both'
				);

$settings = woo_get_dynamic_values( $settings );

$title_before = '<h1 class="title entry-title">';
$title_after = '</h1>';



$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

woo_post_before();
?>

<article <?php post_class(); ?>>
<div class="stripe"><?php the_terms( $post->ID, 'stripes' ); ?></div>

<?php the_post_thumbnail('blog'); ?>

<header>

	

	</header>
	
	<section class="entry">
	<h2 class="title entry-title"><?php the_title(); ?></h2>
<?php the_content(); ?>
	</section><!-- /.entry -->



	
	<div class="fix"></div>
<div class="hidesingle">
  <hr />
</div>
</article><!-- /.post -->