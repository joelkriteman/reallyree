<?php
/*
Plugin Name: Dummy Ratings
Plugin URI: https://www.elementous.com
Description: Adds dummy ratings. Add-on for Rating Manager.
Author: Elementous
Author URI: https://www.elementous.com
Version: 1.0
*/

define( 'ELM_DR_VERSION', '1.0' );

class Elm_Dummy_Ratings {

    public function __construct() {
        add_action( 'init', array( $this, 'init' ), 100 );
    }

    public function init() {
        add_action( 'add_meta_boxes', array( $this, 'meta_box' ) );

        add_action( 'save_post', array( $this, 'save_post' ) );
    }

    public function meta_box() {
        $post_types = apply_filters( 'elm_dm_post_types', array( 'post' ) );

        add_meta_box( 'dummy-ratings', __( 'Dummy Ratings', 'elm' ), array( $this, 'meta_box_callback' ), $post_types, 'side', 'low' );
    }

    public function meta_box_callback() {
    ?>
        <p>
            <label for="dummy-rating-value"><?php _e('Rating value', 'elm'); ?></label><br />
            <input type="number" name="dummy_rating_value" id="dummy-rating-value" max="5" min="0" value="" />
        </p>

        <p>
            <label for="dummy-rating-count"><?php _e('The number of ratings', 'elm'); ?></label><br />
            <input type="number" name="dummy_rating_count" id="dummy-rating-count" min="0" value="" />
        </p>
    <?php
    }

    /*
     * Add ratings on post publish or update.
     */
    public function save_post( $post_id ) {
        if ( isset( $_POST['dummy_rating_value'] ) && ctype_digit( $_POST['dummy_rating_value'] ) && (int) $_POST['dummy_rating_value'] > 0 && isset( $_POST['dummy_rating_count'] ) && ctype_digit( $_POST['dummy_rating_count'] ) && (int) $_POST['dummy_rating_value'] > 0 ) {
            $rating_value = (int) $_POST['dummy_rating_value'];
            $count = (int) $_POST['dummy_rating_count'];

            if ( $rating_value > 5 )
                $rating_value = 5;

            global $elm_ur_ratings;

            for ( $i = 1; $i <= $count; $i++ ) {
                $elm_ur_ratings->add_rating( $post_id, $rating_value );
            }
        }
    }
}

$elm_dummy_ratings = new Elm_Dummy_Ratings();