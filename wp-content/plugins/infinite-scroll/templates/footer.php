 <?php
/**
 * Template to include on wp_footer front-end; passes plugin options to javascript function
 * @package Infinite_Scroll
 */

$additional_js_dir =  plugins_url('/infinite-scroll/js-additional');

?>

<script type="text/javascript" src="<?php echo $additional_js_dir; ?>/matchMedia.js"></script>
<script type="text/javascript" src="<?php echo $additional_js_dir; ?>/matchMedia.addListener.js"></script>
<script type="text/javascript">

// Because the `wp_localize_script` method makes everything a string
infinite_scroll = jQuery.parseJSON(infinite_scroll);


var minWidth = window.matchMedia('(min-width: 651px)');
if(minWidth.matches == true){

  jQuery( infinite_scroll.contentSelector ).infinitescroll( infinite_scroll, function(newElements, data, url) { eval(infinite_scroll.callback); });

}



</script>
