/**
*
* WP Author Box Admin JS
* URL: http://www.codecanyon.net/user/phpbits
* Version: 2.0
* Author: phpbits
* Author URL: http://www.codecanyon.net/user/phpbits
*
*/

// Utility
if ( typeof Object.create !== 'function' ) {
	Object.create = function( obj ) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

(function( $, window, document, undefined ) {
  if ( $.isFunction($.fn.select2) ){
    $('#wpautbox_pro-tabs .tab_type select').select2()
    .live("change", function(e) {
      if(e.val == 'post_type'){
        $(e.target).parent('.redux-field-container').parent('.tab_type').parent('.ui-accordion-content').find('.tab_contact_form').hide();
        $(e.target).parent('.redux-field-container').parent('.tab_type').parent('.ui-accordion-content').find('.tab_post_type').show();
      }else if(e.val == 'form'){
        $(e.target).parent('.redux-field-container').parent('.tab_type').parent('.ui-accordion-content').find('.tab_post_type').hide();
        $(e.target).parent('.redux-field-container').parent('.tab_type').parent('.ui-accordion-content').find('.tab_contact_form').show();
      }else{
        $(e.target).parent('.redux-field-container').parent('.tab_type').parent('.ui-accordion-content').find('.tab_post_type').hide();
        $(e.target).parent('.redux-field-container').parent('.tab_type').parent('.ui-accordion-content').find('.tab_contact_form').hide();
      }
    });
  }

    $('#wpautbox_pro-tabs .tab_type select').each(function(){
    	getVal = $(this).val();
    	if( getVal == 'post_type' ){
    		$(this).parent('.redux-field-container').parent('.tab_type').parent().find('.tab_post_type').show();
    	}else if( getVal == 'form' ){
    		$(this).parent('.redux-field-container').parent('.tab_type').parent().find('.tab_contact_form').show();
    	}

    	console.log(getVal);
    });
})( jQuery, window, document );