<?php
/*
 * WP Author Box Admin Funtions
 * @version 2.0
 */

add_action( 'admin_enqueue_scripts', 'wpautbox_admin_enqueue' );
function wpautbox_admin_enqueue(){
	wp_enqueue_style( 'css-wpautbox-admin', plugins_url( 'lib/css/wpauthbox-admin.css' , dirname(__FILE__) ) , array(), null );
	wp_register_script(
		'jquery-wpautbox-admin',
		plugins_url( 'lib/js/jquery-wpautbox.admin.js' , dirname(__FILE__) ),
		array( 'jquery' ),
		'',
		true
	);

	wp_enqueue_style( 'css-wpautbox-admin' );
	wp_enqueue_script('jquery-wpautbox-admin');
}

?>