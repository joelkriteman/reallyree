<?php

/*
Plugin Name: WP Author Box Pro
Plugin URI: https://phpbits.net/plugin/wp-author-box/
Description: Awesome Author box that you'll fall inlove with. Adds Author box after every post, pages and custom post types.
Version: 2.0.7
Author: phpbits
Author URI: https://phpbits.net/
License: GPL2
*/

//avoid direct calls to this file
if (!function_exists('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

define('WPAUTHBOX_PLUGIN_VERSION', '2.0');

/*##################################
	REQUIRE
################################## */
require_once( dirname( __FILE__ ) . '/includes/loader.php');

//Add Redux Framework
if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/includes/ReduxFramework/ReduxCore/framework.php' ) ) {
	require_once( dirname( __FILE__ ) . '/includes/ReduxFramework/ReduxCore/framework.php' );
}

require_once( dirname( __FILE__ ) . '/includes/socials.php');
require_once( dirname( __FILE__ ) . '/admin/functions-config.php');
require_once( dirname( __FILE__ ) . '/admin/functions.admin.php');
require_once( dirname( __FILE__ ) . '/core/functions.user-setting.php');
require_once( dirname( __FILE__ ) . '/core/functions.display.php');
require_once( dirname( __FILE__ ) . '/core/functions.widget.php');
if(function_exists('wpcf7')){
	require_once( dirname( __FILE__ ) . '/core/functions.contact.form.php');
}
if( class_exists('GFForms') ){
	require_once( dirname( __FILE__ ) . '/core/functions.gravity.form.php');
}

/**
 * Restore Custom Tabs on upgrade
 *
 * @since 2.0
 */
add_action('admin_init', 'wpautbox_version_upgrade');
function wpautbox_version_upgrade(){
	global $wpautbox_pro;
	//transfer options for version 2.0 upgrade
	if(defined('WPAUTHBOX_PLUGIN_VERSION')){
		$version = WPAUTHBOX_PLUGIN_VERSION;
		if( $version == '2.0' ){
			if(!isset($wpautbox_pro['tabs']['redux_group_data']) && isset($wpautbox_pro['tabs']) && !empty($wpautbox_pro['tabs'])){
				$redux_group_data = array();
				$tab_label = array();
				$icon = array();
				$tab_type = array();
				$tab_post_type = array();
				$show_custom_tab = array();

				foreach ($wpautbox_pro['tabs'] as $key => $value) {
					$redux_group_data[$key] = array(
												'title'	=>	$value['slide_title']
											);
					$tab_label[$key] = $value['slide_title'];
					$icon[$key] = $value['select-elusive'];
					$tab_type[$key] = $value['tab_type'];
					$show_custom_tab[$key] = (isset($value['show_custom_tab'])) ? $value['show_custom_tab'] : '0';
				}
				$wpautbox_pro['tabs']['redux_group_data'] = $redux_group_data;
				$wpautbox_pro['custom-tab-label'] = $tab_label;
				$wpautbox_pro['select-elusive'] = $icon;
				$wpautbox_pro['tab_type'] = $tab_type;
				$wpautbox_pro['tab_post_type'] = $tab_post_type;
				$wpautbox_pro['show_custom_tab'] = $show_custom_tab;

				//update options
				update_option('wpautbox_pro', $wpautbox_pro);
			}
		}
	}
}
?>