<?php
/*
 * Add Contact Form 7 Tab Support
 * Thanks to https://wordpress.org/plugins/contact-form-7-dynamic-text-extension/
 * @version 2.0
 */

/**
 ** A base module for [wpauthorboxemail], [wpauthorboxemail*]
 **/
function wpcf7_wpauthorbox_init(){

	if(function_exists('wpcf7_add_shortcode')){

		/* Shortcode handler */		
		wpcf7_add_shortcode( 'wpauthorboxemail', 'wpcf7_wpauthorbox_shortcode_handler', true );
		wpcf7_add_shortcode( 'wpauthorboxemail*', 'wpcf7_wpauthorbox_shortcode_handler', true );

		wpcf7_add_shortcode( 'wpauthorboxname', 'wpcf7_wpauthorboxname_shortcode_handler', true );
		wpcf7_add_shortcode( 'wpauthorboxname*', 'wpcf7_wpauthorboxname_shortcode_handler', true );
	
	}
	
	add_filter( 'wpcf7_validate_wpauthorboxemail', 'wpcf7_wpauthorbox_validation_filter', 10, 2 );
	add_filter( 'wpcf7_validate_wpauthorboxemail*', 'wpcf7_wpauthorbox_validation_filter', 10, 2 );
	add_filter( 'wpcf7_validate_wpauthorboxname', 'wpcf7_wpauthorboxname_validation_filter', 10, 2 );
	add_filter( 'wpcf7_validate_wpauthorboxname*', 'wpcf7_wpauthorboxname_validation_filter', 10, 2 );
	
	add_action( 'admin_init', 'wpcf7_add_tag_generator_wpauthorbox', 80 );
	add_action( 'admin_init', 'wpcf7_add_tag_generator_wpauthorboxname', 80 );
	
}
add_action( 'plugins_loaded', 'wpcf7_wpauthorbox_init' , 20 );

/*************************************************************
 * wpauthorbox Shortcode
 *************************************************************/

function wpcf7_wpauthorbox_shortcode_handler( $tag ) {
	
	$wpcf7_contact_form = wpcf7_get_current_contact_form();

	if ( ! is_array( $tag ) )
		return '';

	$type = $tag['type'];
	$name = $tag['name'];
	$options = (array) $tag['options'];
	$values = (array) $tag['values'];

	if ( empty( $name ) )
		return '';

	$atts = '';
	$id_att = '';
	$class_att = '';
	$size_att = '';
	$maxlength_att = '';
	$tabindex_att = '';

	$class_att .= ' wpcf7-text';

	if ( 'wpauthorboxemail*' == $type )
		$class_att .= ' wpcf7-validates-as-required';

	foreach ( $options as $option ) {
		if ( preg_match( '%^id:([-0-9a-zA-Z_]+)$%', $option, $matches ) ) {
			$id_att = $matches[1];

		} elseif ( preg_match( '%^class:([-0-9a-zA-Z_]+)$%', $option, $matches ) ) {
			$class_att .= ' ' . $matches[1];

		} elseif ( preg_match( '%^([0-9]*)[/x]([0-9]*)$%', $option, $matches ) ) {
			$size_att = (int) $matches[1];
			$maxlength_att = (int) $matches[2];

		} elseif ( preg_match( '%^tabindex:(\d+)$%', $option, $matches ) ) {
			$tabindex_att = (int) $matches[1];

		}
	}

	if ( $id_att )
		$atts .= ' id="' . trim( $id_att ) . '"';

	if ( $class_att )
		$atts .= ' class="' . trim( $class_att ) . '"';

	if ( $size_att )
		$atts .= ' size="' . $size_att . '"';
	else
		$atts .= ' size="40"'; // default size

	if ( $maxlength_att )
		$atts .= ' maxlength="' . $maxlength_att . '"';

	if ( '' !== $tabindex_att )
		$atts .= sprintf( ' tabindex="%d"', $tabindex_att );

	// Value
	if ( is_a( $wpcf7_contact_form, 'WPCF7_ContactForm' ) && $wpcf7_contact_form->is_posted() ) {
		if ( isset( $_POST['_wpcf7_mail_sent'] ) && $_POST['_wpcf7_mail_sent']['ok'] )
			$value = '';
		else
			$value = stripslashes_deep( $_POST[$name] );
	} else {
		$value = isset( $values[0] ) ? $values[0] : '';
	}
	
	$scval = do_shortcode('['.$value.']');
	if($scval != '['.$value.']') $value = $scval;
	
	//echo '<pre>'; print_r($options);echo '</pre>';
	$readonly = '';
	if(in_array('uneditable', $options)){
		$readonly = 'readonly="readonly"';
	}

	$html = '<input type="hidden" name="' . $name . '" value="{{wpauthorbox_email}}"' . $atts . ' '. $readonly.' />';

	$validation_error = '';
	if ( is_a( $wpcf7_contact_form, 'WPCF7_ContactForm' ) )
		$validation_error = $wpcf7_contact_form->validation_error( $name );

	$html = '<span class="wpcf7-form-control-wrap ' . $name . '">' . $html . $validation_error . '</span>';

	return $html;
}

/* Validation filter */

function wpcf7_wpauthorbox_validation_filter( $result, $tag ) {
	
	$wpcf7_contact_form = wpcf7_get_current_contact_form();

	$type = $tag['type'];
	$name = $tag['name'];

	$_POST[$name] = trim( strtr( (string) $_POST[$name], "\n", " " ) );

	if ( 'wpauthorboxemail*' == $type ) {
		if ( '' == $_POST[$name] ) {
			$result['valid'] = false;
			$result['reason'][$name] = $wpcf7_contact_form->message( 'invalid_required' );
		}
	}

	return $result;
}

/* Tag generator */

function wpcf7_add_tag_generator_wpauthorbox() {
	if(function_exists('wpcf7_add_tag_generator')){
		wpcf7_add_tag_generator( 'wpauthorboxemail', __( 'WP Author Box Email Field', 'wpcf7' ),
			'wpcf7-tg-pane-wpauthorboxemail', 'wpcf7_tg_pane_wpauthorbox_' );
	}
}

function wpcf7_tg_pane_wpauthorbox_( $contact_form ) {
	wpcf7_tg_pane_wpauthorbox( 'wpauthorboxemail' );
}

function wpcf7_tg_pane_wpauthorbox( $type = 'wpauthorboxemail' ) {
?>

	<div id="wpcf7-tg-pane-<?php echo $type; ?>">
		<p><?php _e('This will display as hidden text field with the author\'s email address. ','wpautbox')?></p>
		<form action="" class="tag-generator-panel">

		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">
						<?php echo esc_html( __( 'Name', 'wpcf7' ) ); ?>
					</th>
					<td>
						<input type="text" name="name" class="tg-name oneline" />
					</td>
				</tr>
				<tr>
					<th scope="row">
						<code>id</code> (<?php echo esc_html( __( 'optional', 'wpcf7' ) ); ?>)
					</th>
					<td>
						<input type="text" name="id" class="idvalue oneline option" />
					</td>
				</tr>
			</tbody>
		</table>

		<!-- <div class="tg-tag"><?php echo esc_html( __( "Copy this code and paste it into the form left.", 'wpcf7' ) ); ?><br /><input type="text" name="<?php echo $type; ?>" class="tag" readonly="readonly" onfocus="this.select()" /></div>

		<div class="tg-mail-tag"><?php echo esc_html( __( "And, put this code into the Mail fields below.", 'wpcf7' ) ); ?><br />&nbsp;<input type="text" class="mail-tag" readonly="readonly" onfocus="this.select()" /></div> -->
		</form>

		<div class="insert-box">
			<input type="text" name="<?php echo $type; ?>" class="tag code" readonly="readonly" onfocus="this.select()" />

			<div class="submitbox">
			<input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
			</div>

			<br class="clear" />
		</div>
	</div>
<?php
}

/**
 ** A base module for [wpauthorboxname], [wpauthorboxname*]
 **/

function wpcf7_wpauthorboxname_shortcode_handler( $tag ) {
	
	$wpcf7_contact_form = wpcf7_get_current_contact_form();

	if ( ! is_array( $tag ) )
		return '';

	$type = $tag['type'];
	$name = $tag['name'];
	$options = (array) $tag['options'];
	$values = (array) $tag['values'];

	if ( empty( $name ) )
		return '';

	$atts = '';
	$id_att = '';
	$class_att = '';
	$size_att = '';
	$maxlength_att = '';
	$tabindex_att = '';

	$class_att .= ' wpcf7-text';

	if ( 'wpauthorboxname*' == $type )
		$class_att .= ' wpcf7-validates-as-required';

	foreach ( $options as $option ) {
		if ( preg_match( '%^id:([-0-9a-zA-Z_]+)$%', $option, $matches ) ) {
			$id_att = $matches[1];

		} elseif ( preg_match( '%^class:([-0-9a-zA-Z_]+)$%', $option, $matches ) ) {
			$class_att .= ' ' . $matches[1];

		} elseif ( preg_match( '%^([0-9]*)[/x]([0-9]*)$%', $option, $matches ) ) {
			$size_att = (int) $matches[1];
			$maxlength_att = (int) $matches[2];

		} elseif ( preg_match( '%^tabindex:(\d+)$%', $option, $matches ) ) {
			$tabindex_att = (int) $matches[1];

		}
	}

	if ( $id_att )
		$atts .= ' id="' . trim( $id_att ) . '"';

	if ( $class_att )
		$atts .= ' class="' . trim( $class_att ) . '"';

	if ( $size_att )
		$atts .= ' size="' . $size_att . '"';
	else
		$atts .= ' size="40"'; // default size

	if ( $maxlength_att )
		$atts .= ' maxlength="' . $maxlength_att . '"';

	if ( '' !== $tabindex_att )
		$atts .= sprintf( ' tabindex="%d"', $tabindex_att );

	// Value
	if ( is_a( $wpcf7_contact_form, 'WPCF7_ContactForm' ) && $wpcf7_contact_form->is_posted() ) {
		if ( isset( $_POST['_wpcf7_mail_sent'] ) && $_POST['_wpcf7_mail_sent']['ok'] )
			$value = '';
		else
			$value = stripslashes_deep( $_POST[$name] );
	} else {
		$value = isset( $values[0] ) ? $values[0] : '';
	}
	
	$scval = do_shortcode('['.$value.']');
	if($scval != '['.$value.']') $value = $scval;
	
	//echo '<pre>'; print_r($options);echo '</pre>';
	$readonly = '';
	if(in_array('uneditable', $options)){
		$readonly = 'readonly="readonly"';
	}

	$html = '<input type="hidden" name="' . $name . '" value="{{wpauthorbox_name}}"' . $atts . ' '. $readonly.' />';

	$validation_error = '';
	if ( is_a( $wpcf7_contact_form, 'WPCF7_ContactForm' ) )
		$validation_error = $wpcf7_contact_form->validation_error( $name );

	$html = '<span class="wpcf7-form-control-wrap ' . $name . '">' . $html . $validation_error . '</span>';

	return $html;
}

/* Validation filter */

function wpcf7_wpauthorboxname_validation_filter( $result, $tag ) {
	
	$wpcf7_contact_form = wpcf7_get_current_contact_form();

	$type = $tag['type'];
	$name = $tag['name'];

	$_POST[$name] = trim( strtr( (string) $_POST[$name], "\n", " " ) );

	if ( 'wpauthorboxname*' == $type ) {
		if ( '' == $_POST[$name] ) {
			$result['valid'] = false;
			$result['reason'][$name] = $wpcf7_contact_form->message( 'invalid_required' );
		}
	}

	return $result;
}

/* Tag generator */

function wpcf7_add_tag_generator_wpauthorboxname() {
	if(function_exists('wpcf7_add_tag_generator')){
		wpcf7_add_tag_generator( 'wpauthorboxname', __( 'WP Author Box Display Name', 'wpcf7' ),
			'wpcf7-tg-pane-wpauthorboxname', 'wpcf7_tg_pane_wpauthorboxname_' );
	}
}

function wpcf7_tg_pane_wpauthorboxname_( $contact_form ) {
	wpcf7_tg_pane_wpauthorboxname( 'wpauthorboxname' );
}

function wpcf7_tg_pane_wpauthorboxname( $type = 'wpauthorboxname' ) {
?>

	<div id="wpcf7-tg-pane-<?php echo $type; ?>">
		<p><?php _e('This will display as hidden text field with the author\'s display name value. ','wpautbox')?></p>
		<form action="">

		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">
						<?php echo esc_html( __( 'Name', 'wpcf7' ) ); ?>
					</th>
					<td>
						<input type="text" name="name" class="tg-name oneline" />
					</td>
				</tr>
				<tr>
					<th scope="row">
						<code>id</code> (<?php echo esc_html( __( 'optional', 'wpcf7' ) ); ?>)
					</th>
					<td>
						<input type="text" name="id" class="idvalue oneline option" />
					</td>
				</tr>
			</tbody>
		</table>


		<!-- <div class="tg-tag"><?php echo esc_html( __( "Copy this code and paste it into the form left.", 'wpcf7' ) ); ?><br /><input type="text" name="<?php echo $type; ?>" class="tag" readonly="readonly" onfocus="this.select()" /></div>

		<div class="tg-mail-tag"><?php echo esc_html( __( "And, put this code into the Mail fields below.", 'wpcf7' ) ); ?><br />&nbsp;<input type="text" class="mail-tag" readonly="readonly" onfocus="this.select()" /></div> -->
		</form>

		<div class="insert-box">
			<input type="text" name="<?php echo $type; ?>" class="tag code" readonly="readonly" onfocus="this.select()" />

			<div class="submitbox">
			<input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
			</div>

			<br class="clear" />
		</div>
	</div>
<?php
}
?>