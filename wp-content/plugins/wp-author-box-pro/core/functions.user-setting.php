<?php

/*##################################
	USER SETTINGS
################################## */

/**
 * Additional user fields for Author Box 
 *
 * @since 1.0
 */
class wpautbox_author_settings{
	function __construct(){

		add_action( 'edit_user_profile', array($this, 'user_fields') );
		add_action( 'show_user_profile', array($this, 'user_fields') );
		add_action( 'personal_options_update', array($this,'wpautbox_save_profile_fields') );
		add_action( 'edit_user_profile_update', array($this,'wpautbox_save_profile_fields') );
	}

	function user_fields( $user ){
		global $wpautbox_pro;
		$user_roles = $user->roles;
		$user_role = array_shift($user_roles);
		if( array_key_exists( $user_role, $wpautbox_pro['user_levels']) && 1 == $wpautbox_pro['user_levels'][$user_role] ){
			$wpautbox_socials = apply_filters('wpautbox-socials',array()); // REMOVE LATER
	        $wpautbox_socials = apply_filters('wpautbox/socials',$wpautbox_socials);
			$wpautbox_meta = get_user_meta( $user->ID, 'wpautbox_user_fields', false );
			$wpautbox_user_meta = array(); //initialize user meta variable
			if(!empty( $wpautbox_meta )){
				$wpautbox_meta = unserialize( base64_decode($wpautbox_meta[0]) );
				if(isset( $wpautbox_meta['user'] )){
					$wpautbox_user_meta = $wpautbox_meta['user'];
				}
			}
			$this->wpautbox_upload_js();
			?>
			<h3><?php _e( 'WP Author Box User Fields', 'wpautbox' ); ?></h3>

			<table class="form-table">
				<tbody>
					<tr>
						<th><label for="wpautbox_user_disable"><?php _e( 'Disable Author Box', 'wpautbox' ); ?></label></th>
						<td>
							<label for="wpautbox_user_disable">
								<input type="checkbox" name="wpautbox[user][disable]" id="wpautbox_user_disable" value="true" <?php if(isset( $wpautbox_user_meta['disable'] ) && !empty( $wpautbox_user_meta['disable'] ) ){ echo 'checked="checked"'; } ?> />
								<span class="description"><?php _e( 'Do not show Author Box in your posts, pages and custom posts', 'wpautbox' ); ?></span>
							</label>
						</td>
					</tr>
					<tr>
						<th><label for="wpautbox_user_avatar"><?php _e( 'Profile Image', 'wpautbox' ); ?></label></th>
						<td>
							<span id="wpautbox_user_image_url">
								<?php 
									if(isset( $wpautbox_user_meta['image'] ) && !empty( $wpautbox_user_meta['image'] ) ){
										$image_attributes = wp_get_attachment_image_src( $wpautbox_user_meta['image'] ); // returns an array
											if( $image_attributes ) {
												echo '<img src="'. $image_attributes[0] .'" width="120"><br />';
											}
									} 
								?>
							</span>
							<input type="hidden" name="wpautbox[user][image]" id="wpautbox_user_image" value="<?php if(isset( $wpautbox_user_meta['image'] ) && !empty( $wpautbox_user_meta['image'] ) ){ echo $wpautbox_user_meta['image']; } ?>" class="regular-text" />  
							<input type='button' class="button-primary" value="Upload Image" id="wpautbox_upload_image" data-uploader_title="<?php _e( 'Choose Profile Image', 'wpautbox' ); ?>" data-uploader_button_text="<?php _e( 'Use as Profile Image', 'wpautbox' ); ?>" />
							 <input type="button" id="wpautbox_remove_image" class="button-secondary" value="Remove Image" />
							<br />
							<span class="description"><?php _e( 'Please upload your image for your profile.', 'wpautbox' ); ?></span>
						</td>
					</tr>
					<?php if( isset($wpautbox_pro['tabs']) && !empty($wpautbox_pro['tabs'])){
						$settings = array( 'media_buttons' => false );
						$tab_count = 0;
						foreach ($wpautbox_pro['tabs']['redux_group_data'] as $tab_key => $tab_value) {
							if( isset($wpautbox_pro['tab_type'][$tab_count]) && ('html' == $wpautbox_pro['tab_type'][$tab_count]) ){ 
								$tab_content = (isset($wpautbox_user_meta['tabs'][ $tab_key ])) ? $wpautbox_user_meta['tabs'][$tab_key ] : '';
								$tab_content = stripslashes($tab_content);
								$settings['textarea_name'] = 'wpautbox[user][tabs]['. $tab_key .']';
								?>
								<tr>
									<th><label for="wpautbox_user_tab_<?php echo $tab_key;?>"><?php _e( $wpautbox_pro['custom-tab-label'][$tab_count], 'wpautbox' ); ?></label></th>
									<td>
										<?php wp_editor( $tab_content, 'wpautbox_user_tabs_'. $tab_key , $settings );?>
										<label for="wpautbox_user_tab_<?php echo $tab_key;?>">
											<span class="description"><?php _e( 'Add Custom Text, html or shortcode here.', 'wpautbox' ); ?></span>
										</label>
									</td>
								</tr>
							<?php }
							if( isset($wpautbox_pro['tab_type'][$tab_count]) && ('form' == $wpautbox_pro['tab_type'][$tab_count]) ){ ?>
								<tr>
									<th><label for="wpautbox_user_tab_<?php echo $tab_key;?>"><?php _e( 'Hide ' . $wpautbox_pro['custom-tab-label'][$tab_count], 'wpautbox' ); ?></label></th>
									<td>
										<input type="checkbox" name="wpautbox[user][tabs][<?php echo $tab_key;?>]" id="wpautbox_user_tab_<?php echo $tab_key;?>" value="1" <?php if(isset($wpautbox_user_meta['tabs'][$tab_key ]) && $wpautbox_user_meta['tabs'][$tab_key ] == '1'){ echo 'checked="checked"'; }?> /> <small><?php _e( 'Check to hide this Tab', 'wpautbox' ); ?></small>
									</td>
								</tr>
							<?php
							}
							$tab_count++;
						}
					}?>
					<?php 
					if(isset($wpautbox_pro['social_display']) && 1 == $wpautbox_pro['social_display'] && !empty($wpautbox_pro['socials'])){
						foreach($wpautbox_pro['socials'] as $key => $field):
						if(!empty($wpautbox_pro['socials']) && isset($wpautbox_pro['socials'][$key]) && 1 == $wpautbox_pro['socials'][$key]):
					?>
					<tr>
						<th><label for="wpautbox_user_<?php echo $key;?>"><?php echo $wpautbox_socials[ $key ]; ?></label></th>
						<td>
							<input type="text" class="regular-text" id ="wpautbox_user_<?php echo $key;?>" name="wpautbox[user][socials][<?php echo $key;?>]" value="<?php if(isset( $wpautbox_user_meta['socials'][ $key ] )){ echo  $wpautbox_user_meta['socials'][ $key ]; };?>"><br />
							<?php if( 'tagline' == $key ) :?>
								<span class="description"><?php _e('Your Profile Tagline','wpautbox');?></span>
							<?php else:?>
								<span class="description"><?php _e('Your ' . $wpautbox_socials[ $key ] . ' Profile Link','wpautbox');?></span>
							<?php endif;?>
						</td>
					</tr>
					<?php endif; endforeach; }?>
				</tbody>
			</table>

		<?php
		}
	}
	function wpautbox_save_profile_fields( $user_id ) {
		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;

		if(isset( $_POST['wpautbox'] )){
			$wpautbox_meta = $_POST['wpautbox'];
			
			foreach ($wpautbox_meta['user'] as $key => $value) {
				if( 'socials' == $key ){
					foreach ($value as $social_key => $social_value) {
						if(!empty( $social_value )){
							if($social_key == 'mail' || $social_key == 'skype'){
								$wpautbox_meta['user'][ $key ][ $social_key ] = $social_value;
							}else{
								$wpautbox_meta['user'][ $key ][ $social_key ] = $this->wpautbox_addhttp( $social_value );
							}
						}
					}
				}
				if( 'tabs' == $key ){
					foreach ($value as $tab_key => $tab_value) {
						if(!empty( $tab_value )){
							$wpautbox_meta['user'][ $key ][ $tab_key ] = $tab_value;
						}
					}
				}
			}
			$wpautbox_meta = base64_encode(serialize( $wpautbox_meta ));
			update_user_meta( $user_id, 'wpautbox_user_fields', $wpautbox_meta );
		}
	}

	/**
	 * Add http to the url values
	 *
	 * @since 1.0
	 */
	function wpautbox_addhttp($url) {
	    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
	        $url = "http://" . $url;
	    }
	    return $url;
	}

	/*
	 * Image Upload Script
	 * since version 1.1
	 */
	function wpautbox_upload_js() { 
		//add media upload library
		wp_enqueue_media(); 
		?>
		<script type="text/javascript">
		jQuery(document).ready(function() 
		{
			// Uploading files
			var file_frame;

			  jQuery('#wpautbox_upload_image').live('click', function( event ){

			    event.preventDefault();

			    // If the media frame already exists, reopen it.
			    if ( file_frame ) {
			      file_frame.open();
			      return;
			    }

			    // Create the media frame.
			    file_frame = wp.media.frames.file_frame = wp.media({
			      title: jQuery( this ).data( 'uploader_title' ),
			      button: {
			        text: jQuery( this ).data( 'uploader_button_text' ),
			      },
			      multiple: false  // Set to true to allow multiple files to be selected
			    });

			    // When an image is selected, run a callback.
			    file_frame.on( 'select', function() {
			      // We set multiple to false so only get one image from the uploader
			      attachment = file_frame.state().get('selection').first().toJSON();
			      jQuery('#wpautbox_user_image').val(attachment.id);
			      jQuery('#wpautbox_user_image_url').html('<img src="'+ attachment.url +'" width="120"/><br />');
			      // Do something with attachment.id and/or attachment.url here
			    });

			    // Finally, open the modal
			    file_frame.open();
			  });
				
				jQuery('#wpautbox_remove_image').live('click',function(){
					jQuery('#wpautbox_user_image').val('');
					jQuery('#wpautbox_user_image_url').html('');
				});
		});
		</script>
	<?php
	}
}
$wpautbox_author_settings = new wpautbox_author_settings();
?>