<?php
/*##################################
	AUTHOR BOX DISPLAY
################################## */

class wpautbox_display{
	public $post;
	public $authordata;
	public $settings;
	public $unique;

	function __construct(){
		global $post;
		global $authordata;
		global $settings;
		global $show_in;

		$this->unique = uniqid();
		add_filter( 'the_content', array($this, 'build_content'), 10 );
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue') );
		add_action('wp_head', array($this, 'wpautbox_wp_head'));
		add_action('wp_head', array($this, 'facebook_author_meta'));
		add_action('init', array($this, 'register_shortcodes'));
		add_action("wp_ajax_wpautbox_ajax", array( $this, "wpautbox_ajax" ));
		add_action("wp_ajax_nopriv_wpautbox_ajax", array( $this, "wpautbox_ajax" ));
	}

	function enqueue(){
		global $wpautbox_pro;
		$this->settings = $wpautbox_pro;

		wp_enqueue_style( 'css-wpautbox-tab', plugins_url( 'lib/css/jquery-a-tabs.css' , dirname(__FILE__) ) , array(), null );
		wp_enqueue_style( 'css-wpautbox', plugins_url( 'lib/css/wpautbox.css' , dirname(__FILE__) ) , array(), null );

		//enqueue skin
		if( isset($this->settings['tab_skin']) && !empty($this->settings['tab_skin']) 
		 	&& "default" != $this->settings['tab_skin'] && "custom" != $this->settings['tab_skin']){
			wp_enqueue_style( 'css-wpautbox-' . $this->settings['tab_skin'], plugins_url( 'lib/css/'. $this->settings['tab_skin'] .'.css' , dirname(__FILE__) ) , array(), null );
		}

		wp_register_style(
            'wpautbox-elusive-icon',
            plugins_url( 'includes/ReduxFramework/ReduxCore/assets/css/vendor/elusive-icons/elusive-icons.css' , dirname(__FILE__) ),
            array(),
            '' ,
            'all'
        );

        wp_register_style(
            'wpautbox-elusive-icon-ie7',
            plugins_url( 'includes/ReduxFramework/ReduxCore/assets/css/vendor/elusive-icons/elusive-webfont-ie7.css' , dirname(__FILE__) ),
            array(),
            plugins_url( 'includes/ReduxFramework/ReduxCore/assets/css/vendor/elusive-icons/elusive-webfont-ie7.css' , dirname(__FILE__) ) ,
            'all'
        );

		wp_register_script(
			'jquery-wpautbox-tab',
			plugins_url( 'lib/js/jquery.a-tab.js' , dirname(__FILE__) ),
			array( 'jquery' ),
			'',
			true
		);
		wp_register_script(
			'jquery-wpautbox-pro',
			plugins_url( 'lib/js/jquery.wpautbox.js' , dirname(__FILE__) ),
			array( 'jquery' ),
			'',
			true
		);

		wp_enqueue_script('jquery-wpautbox-tab');
		wp_enqueue_script('jquery-wpautbox-pro');
		wp_localize_script( 'jquery-wpautbox-pro', 'wpautboxAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_style( 'wpautbox-elusive-icon' );
		// wp_enqueue_style( 'wpautbox-elusive-icon-ie7' );
	}

	function build_content($content){
		$return = $content;
			if( ( is_single() && in_the_loop() ) || is_page() && in_the_loop() ){
			global $post, $authordata, $wpautbox_pro;
			$this->post = $post;
			$this->authordata = $authordata;
			$this->settings = $wpautbox_pro;
			$this->allowed_post_types();

			$user_roles = $authordata->roles;
			if(is_array($user_roles)){
				$user_role = array_shift($user_roles);
			}else{
				$user_role = array();
			}
			

			 //initialize return element
			if( array_key_exists( $user_role, $wpautbox_pro['user_levels']) && 1 == $wpautbox_pro['user_levels'][$user_role] ){
				if( isset($this->show_in[ $this->post->post_type ]) && 'no' != $this->show_in[ $this->post->post_type ] ){
					switch ( $this->show_in[ $this->post->post_type ] ) {
						case 'above':
							$return = $this->wpauthbox_coauthor('above', $this->authordata->ID, $this->post->ID);
							$return .= $content;
							break;

						case 'below':
							$return = $content;
							$return .= $this->wpauthbox_coauthor('below', $this->authordata->ID, $this->post->ID);
							break;

						case 'both':
							$return = $this->wpauthbox_coauthor('above', $this->authordata->ID, $this->post->ID);
							$return .= $content;
							$return .= $this->wpauthbox_coauthor('below', $this->authordata->ID, $this->post->ID);
							break;

						default:
							$return = $content;
							break;
					}
				}
			}
		}
		return $return;
	}

	function allowed_post_types(){
		$allowed = array();
		$get_cpt_args = array(
			'public'   => true,
		); 
		$custom_post_types = get_post_types( $get_cpt_args, 'names', 'and');
		if(!empty($custom_post_types)):
			foreach( $custom_post_types as $type ){
				$custom_post_type_object = get_post_type_object( $type );
				$key = 'show_in_' . $custom_post_type_object->name;
				if( array_key_exists($key, $this->settings) ){
					$allowed[ $custom_post_type_object->name] = $this->settings[ $key ];
				}
			}
		$this->show_in = $allowed; 
		endif;
	}

	/**
	 * Add Author Box Tabs
	 *
	 * @since 1.0
	 */
	function navtabs($tabs = array()){
		$html = '<ul class="a-tab-nav">';
			if( ( isset($tabs['hide']) && !in_array('about_tab', $tabs['hide']) ) ){
				$html .= '<li class="a-tab-active"><a href="#wpautbox_about"><i class="el-icon-user wpautbox-icon"></i> '. __( $this->settings['about-tab-label'] , 'wpautbox') .'</a></li>';
			}
			if( ( isset($tabs['hide']) && !in_array('latest_tab', $tabs['hide']) ) ){
				$html .= '<li><a href="#wpautbox_latest-post"><i class="el-icon-list wpautbox-icon"></i> '. __($this->settings['latest-tab-label'], 'wpautbox') .'</a></li>';
			}
			if (isset($this->settings['tabs']) && !empty($this->settings['tabs'])) {
				$tab_count = 0;
				foreach ($this->settings['tabs']['redux_group_data'] as $key => $value) {
					if( ( isset($tabs['hide']) && !in_array('custom_tab_'. $tab_count, $tabs['hide']) && !empty($this->settings['tab_type'][$tab_count]) && $this->settings['show_custom_tab'][$tab_count] == '1' )
					){
						$html .= '<li class="wpautbox-nav-custom-tab-'. $tab_count .'"><a href="#wpautbox_custom-tab-'. $tab_count .'">';
						if(isset($this->settings['select-elusive'][$tab_count]) && !empty($this->settings['select-elusive'][$tab_count])){
							$html .= '<i class="'. $this->settings['select-elusive'][$tab_count] .' wpautbox-icon"></i> ';	
						}
						$html .= __($this->settings['custom-tab-label'][$tab_count], 'wpautbox') .'</a></li>';
					}
					$tab_count++;	
				}
			}
		$html .= '</ul>';

		return $html;
	}

	/**
	 * Add Author Box Contents
	 *
	 * @since 1.0
	 */
	function nav_contents($tabs = array()){
		if(!isset($tabs['authorid']) || empty($tabs['authorid'])){
			$tabs['authorid'] = $this->authordata->ID;
		}
		$html = '<div class="a-tab-container">';
			if( ( isset($tabs['hide']) && !in_array('about_tab', $tabs['hide']) ) ){
				$html .= '<div class="a-tab-content" id="wpautbox_about">';
					$html .= $this->authorbio($tabs['authorid']);
				$html .= '</div>';
			}
			if( ( isset($tabs['hide']) && !in_array('latest_tab', $tabs['hide']) ) ){
				$html .= '<div class="a-tab-content" id="wpautbox_latest-post">';
					$html .= $this->custom_tab('post_type', array('post_type' => 'post', 'authorid' => $tabs['authorid']));
				$html .= '</div>';
			}
			if (isset($this->settings['tabs']) && !empty($this->settings['tabs'])) {
				$tab_count = 0;
				foreach ($this->settings['tabs']['redux_group_data'] as $key => $value) {
					if( ( isset($tabs['hide']) && !in_array('custom_tab_'. $key, $tabs['hide']) && !empty($this->settings['tab_type'][$tab_count]) )
					 ){
						$html .= '<div class="a-tab-content" id="wpautbox_custom-tab-'. $tab_count .'">';
						$contents = $this->custom_tab( $this->settings['tab_type'][ $tab_count ] , array('post_type' => $this->settings['tab_post_type'][ $tab_count ] , 'authorid' => $tabs['authorid'], 'tab_key' => $key ));
							//hide tab if content is empty
							if(empty($contents)){
								$contents = '<style> #wpautbox-'. $this->unique .' .wpautbox-nav-custom-tab-'. $tab_count .'{ display:none !important; } </style>';
							}
							$html .= $contents;
						$html .= '</div>';
					}
					$tab_count++;
				}
			}
		$html .= $this->build_socials( $tabs['authorid'] );
		$html .= '</div>';

		return $html;
	}

	/**
	 * Author Bio
	 *
	 * @since 1.0
	 */
	function authorbio($authorid){
		if(!empty($authorid)){
			if(is_array($authorid)){
				$authorid = $authorid['authorid'];
			}
			$html = '';
			$author = get_userdata( $authorid );
			$wpautbox_meta = get_user_meta( $authorid, 'wpautbox_user_fields', false );
			$user_url = get_the_author_meta('user_url', $authorid);
			if(isset($wpautbox_meta[0])){
				$wpautbox_meta = unserialize( base64_decode($wpautbox_meta[0]) );
			}
			if(isset($wpautbox_meta['user']['image']) && !empty($wpautbox_meta['user']['image'])){
				$image_attributes = wp_get_attachment_image_src( $wpautbox_meta['user']['image'], array(120,120) );
				$avatar = '<img src="'. $image_attributes[0] .'" />';
			}else{
				$avatar = get_avatar( $authorid, 120 );
			}
			
			if(!empty($avatar)){
				$html .= '<div class="wpautbox-avatar wpautbox-avatar-'. $this->settings['avatar_shape'] .'">';
					$html .= '<a href="'. get_author_posts_url($authorid) .'">' . $avatar . '</a>';
				$html .= '</div>';
			}

			$html .= '<div class="wpautbox-author-meta">';
				$html .= '<h4 class="wpautbox-name"><span>'. $this->settings['about-title-text'] . '</span> <a href="'. get_author_posts_url($authorid) .'">' . get_the_author_meta( 'display_name', $authorid )  .'</a></h4>';
				if(!empty($wpautbox_meta['user']['wpautbox_tagline'])){
					$html .= '<span class="wpautbox-tagline">'. __( $wpautbox_meta['user']['wpautbox_tagline'] , 'wpautbox') .'</span>';
				}
				$html .= wpautop( get_the_author_meta('description', $authorid) );
				if(!empty($user_url)){
					$html .= '<a href="'. $user_url .'" target="_blank">'.__($this->settings['website-link-label'], 'wpautbox') .'</a>';
				}
			$html .= '</div>';
			return $html;
		}
	}

	/**
	 * Build Custom Tabs for Content and Post Types
	 *
	 * @since 1.0
	 */
	function custom_tab($type, $custom){
		$html = '';
		switch ($type) {
			case 'html':
				if( isset($custom['tab_key']) ){
					if( isset($custom['authorid']) ){
						$authorid = $custom['authorid'];
					}else{
						$authorid = $this->authordata->ID;
					}
					$wpautbox_meta = get_user_meta( $authorid, 'wpautbox_user_fields', false );
					$wpautbox_meta = isset($wpautbox_meta[0]) ? unserialize( base64_decode($wpautbox_meta[0]) ) : array();
					if(isset($wpautbox_meta['user']['tabs']) && isset($wpautbox_meta['user']['tabs'][ $custom['tab_key'] ])){
						$html .= wpautop( stripslashes($wpautbox_meta['user']['tabs'][ $custom['tab_key'] ]) );
					}
				}
				break;
			case 'post_type' :
				if( isset($custom['authorid']) ){
					$authorid = $custom['authorid'];
				}else{
					$authorid = $this->authordata->ID;
				}
				if(!empty($authorid) && isset( $custom['post_type'] )):
				 	$args = array( 
				 				'post_type'			=>	$custom['post_type'],
				 				'post_status'		=>	'publish',
				 				'author'			=>	$authorid,
				 				'posts_per_page' 	=>	$this->settings['latest-posts-num']
				 			);
				 	$obj = get_post_type_object( $custom['post_type'] );
				 	$the_query = new WP_Query( $args );
				 	// The Loop
					if ( $the_query->have_posts() ) {
					        $html .= '<ul class="wpautbox-post_type-list wpautbox-latest-'. $custom['post_type'] .'">';
						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							$html .= '<li><a href="'. get_permalink( get_the_ID() ) .'">' . get_the_title() . '</a> <span class="wpautbox-date">- '. get_the_date() .'</span></li>';
						}
					        $html .= '</ul>';

					        if( 'post' == $custom['post_type'] && ( isset($this->settings['latest-tab-link-text']) && !empty($this->settings['latest-tab-link-text']) ) ){
					        	$archive = get_author_posts_url( $authorid );
					        	$html .= '<a href="'. $archive .'" class="wpautbox-all'. $custom['post_type'] .'">'. $this->settings['latest-tab-link-text'] .'</a>';
					        }
					        
					} else {
						$html = __('No Posts for this author.', 'wpautbox');
					}
				 	wp_reset_postdata();
			 	endif;
				break;
			case 'form' :
				if(function_exists('wpcf7') || class_exists('GFForms')){
					if( isset($custom['tab_key']) ){
						if( isset($custom['authorid']) ){
							$authorid = $custom['authorid'];
						}else{
							$authorid = $this->authordata->ID;
						}
						$wpautbox_meta = get_user_meta( $authorid, 'wpautbox_user_fields', false );
						$wpautbox_meta =  isset($wpautbox_meta[0]) ? unserialize( base64_decode($wpautbox_meta[0]) ) : array();
						$get_email = get_the_author_meta( 'user_email', $custom['authorid'] );
						if( !isset($wpautbox_meta['user']['tabs'][ $custom['tab_key'] ]) || (isset($wpautbox_meta['user']['tabs'][ $custom['tab_key'] ]) && $wpautbox_meta['user']['tabs'][ $custom['tab_key'] ] != '1') ){
							if(!empty($this->settings['tab_contact_form'][$custom['tab_key']])){
								$form = do_shortcode( $this->settings['tab_contact_form'][$custom['tab_key']] );
								$form = str_replace('{{wpauthorbox_email}}', $get_email, $form);
								$form = str_replace('{{wpauthorbox_name}}', get_the_author_meta( 'display_name', $custom['authorid'] ) , $form);
								$html .= $form;
							}
						}
						
					}
				}
				
				break;	
			default:
				# code...
				break;
		}
		return $html;
	}
	/**
	 * Build Social Media icons
	 *
	 * @since 1.0
	 */
	function build_socials( $authorid ){
		$html = '';
		if(isset( $this->settings['socials'] ) && !empty($this->settings['socials']) && 1 == $this->settings['social_display']){
			$wpautbox_meta = get_user_meta( $authorid, 'wpautbox_user_fields', false );
			$wpautbox_user_meta = array(); //initialize user meta variable
			if(!empty( $wpautbox_meta )){
				$wpautbox_meta = unserialize( base64_decode($wpautbox_meta[0]) );
				if(isset( $wpautbox_meta['user'] )){
					$wpautbox_user_meta = $wpautbox_meta['user'];
				}
			}
			if(isset($wpautbox_user_meta['socials'])):
				$count_values = array_filter($wpautbox_user_meta['socials']);
				$count = count($count_values);

				//hide if no social links defined
				if($count > 0){
					$html = '<div class="wpautbox-socials wpautbox-socials-'. $this->settings['social_style'] .' wpautbox-socials-'. $this->settings['social_color'] .'">';
					foreach ($this->settings['socials'] as $key => $value) {
						if( isset($wpautbox_user_meta['socials'][$key]) && !empty($wpautbox_user_meta['socials'][$key]) && ( !empty($value) && '1' == $value )){
							$label = ( 'textonly' == $this->settings['social_style'] ) ? ucfirst( __($key, 'wpautbox') ) : '';
							$classes = ( 'textonly' == $this->settings['social_style'] ) ? 'wpautbox-textonly' : 'wpautbox-icon';
							if('googleplus' == $key){
								$html .= '<a rel="author" href="'. $wpautbox_user_meta['socials'][$key] .'?rel=author" target="_blank" class="wpautbox-google" data-toggle="tooltip" data-original-title="'. __('Google+', 'wpautbox' ) .'" ><span class="'. $classes .' wpautbox-icon-googleplus">'. $label .'</span></a> ';
							}elseif('mail' == $key){
								$html .= '<a href="mailto:'. $wpautbox_user_meta['socials'][$key] .'" class="wpautbox-'. $key .'" data-toggle="tooltip" data-original-title="'. ucfirst( __($key, 'wpautbox') ) .'" ><span class="'. $classes .' wpautbox-icon-'. $key .'">'. $label .'</span></a> ';
							}elseif('skype' == $key){
								$html .= '<a href="skype:'. $wpautbox_user_meta['socials'][$key] .'" class="wpautbox-'. $key .'" data-toggle="tooltip" data-original-title="'. ucfirst( __($key, 'wpautbox') ) .'" ><span class="'. $classes .' wpautbox-icon-'. $key .'">'. $label .'</span></a> ';
							}else{
								$html .= '<a href="'. $wpautbox_user_meta['socials'][$key] .'" target="_blank" class="wpautbox-'. $key .'" data-toggle="tooltip" data-original-title="'. ucfirst( __($key, 'wpautbox') ) .'" ><span class="'. $classes .' wpautbox-icon-'. $key .'">'. $label .'</span></a> ';
							}
						}
					}
					$html .= '</div>';
				}
			endif;
		}

		return $html;
	}

	/**
	 * Build Author Box Tabs
	 *
	 * @since 1.0
	 */
	function construct_authorbox($position = 'below', $authorid, $hide = array(), $type = ''){
		global $post;
		$disabled = false;
		$userdata = get_userdata( $authorid );
		if( isset($userdata->roles) && is_array($userdata->roles)){
			$user_role = array_shift($userdata->roles);
		}else{
			$user_role = (isset($userdata->roles)) ? $userdata->roles : '';
		}

		if(empty($type)){ // fix hidden tabs when not shortcode
			if(!$this->settings['show-about']){
				$hide[] = 'about_tab';
			}
			if(!$this->settings['show-latest-posts']){
				$hide[] = 'latest_tab';
			}
			if (isset($this->settings['tabs']) && !empty($this->settings['tabs'])) {
				$tab_count = 0;
				foreach ($this->settings['tabs'] as $key => $value) {
					if($this->settings['show_custom_tab'][$tab_count] != "1"){
						$hide[] = 'custom_tab_'. $tab_count;
					}
					$tab_count++;
				}
			}

			$wpautbox_meta = get_user_meta( $authorid, 'wpautbox_user_fields', false );
			$wpautbox_user_meta = array(); //initialize user meta variable
			if(!empty( $wpautbox_meta )){
				$wpautbox_meta = unserialize( base64_decode($wpautbox_meta[0]) );
				if(isset( $wpautbox_meta['user'] )){
					$wpautbox_user_meta = $wpautbox_meta['user'];
				}
			}
			if(!empty($wpautbox_user_meta) && isset($wpautbox_user_meta['disable']) && 'true' == $wpautbox_user_meta['disable']){
				$disabled = true;
			}

			//hide by user roles
			$settings_role = $this->settings['user_levels'];
			if(($key = array_search('0', $settings_role)) !== false) {
			    unset($settings_role[$key]);
			}
			$settings_role = array_filter($settings_role);
			if(!array_key_exists( $user_role, $settings_role)){
				$disabled = true;
			}

			// hide authorbox on certain category as of 1.1
			if(isset($post->ID) && !empty($post->ID)){
				//get post categories
				$post_categories = wp_get_post_categories( $post->ID );
				if(isset($this->settings['hide_in_categories']) && !empty($this->settings['hide_in_categories'])){
					foreach ($this->settings['hide_in_categories'] as $cat_key => $cat_value) {
						if(in_array($cat_value, $post_categories)){
							$disabled = true;
							break;
						}
					}
				}
				//hide per post as of version 1.1
				$post_meta = get_post_meta($post->ID, 'wpautbox', true);
				if('false' == $post_meta){
					$disabled = true;
				}
			}
		}
		$html = '<div id="wpautbox-'. $this->unique . '" class="wpautbox-'. $position .'">';
			$html .= '<input type="hidden" class="wpautbox-input-authorid" value="'. $authorid .'" />';
			$html .= $this->navtabs( array( 'hide' => $hide ) );
			$html .= $this->nav_contents( array('authorid' => $authorid, 'hide' => $hide ) );
		$html .= '</div>';

		//reset unique
		$this->unique = uniqid();

		if(!$disabled){
			return $html;
		}
	}

	/**
	 * Build Shortcode
	 *
	 * @since 1.0
	 */
	function build_shortcode($atts, $content = null){
		extract(shortcode_atts(array(
		  'position'		=>	'below',
	      'authorid'		=>	null,
	      'hide'			=> 	array(),
	   ), $atts));

		if(!empty($hide)){
			$hide = explode(',', $hide);
		}
		if( $authorid < 0 ){
			if(isset($this->authordata->ID)){
				$authorid = $this->authordata->ID;
			}else{
				global $post;
				$authorid = $post->post_author;
			}
		}
		
		if(!empty($authorid) && $authorid > 0){
			return $this->construct_authorbox($position, $authorid, $hide, 'shortcode');
		}
	}
	function register_shortcodes(){
	   add_shortcode('wpautbox', array($this, 'build_shortcode'));
	}

	/**
     * Get the Open Graph for the current author
     * @since 1.1
     */
    function facebook_author_meta() {
    	global $post;
    	if(isset($post->post_author) && !empty($post->post_author )){
    		$wpautbox_meta = get_user_meta( $post->post_author , 'wpautbox_user_fields', false );
	    	$wpautbox_user_meta = '';
	    	if(!empty( $wpautbox_meta )){
				$wpautbox_meta = unserialize( base64_decode($wpautbox_meta[0]) );
				if(isset( $wpautbox_meta['user'] )){
					$wpautbox_user_meta = $wpautbox_meta['user'];
				}
			}
			if(!empty($wpautbox_user_meta) && !empty($wpautbox_user_meta['socials']['facebook']) && is_single() ){
				echo "\n" . '<meta property="article:author" content="' . $wpautbox_user_meta['socials']['facebook'] . '" />' . "\n";
			}
    	}
    }

	/**
	 * Add Appearance Options value on custom css
	 *
	 * @since 1.0
	 */
	function wpautbox_wp_head(){
		global $wpautbox_pro;
		$style = '<style type="text/css">';
		if(isset($wpautbox_pro['tab_shadow']) && 0 == $wpautbox_pro['tab_shadow']){
			$style .= 'body .a-tabs .a-tab-container, body .a-tabs>ul.a-tab-nav>li.a-tab-first,body .a-tabs>ul.a-tab-nav>li.a-tab-last{ -webkit-box-shadow: none; -moz-box-shadow: none; box-shadow: none; }';
		}
		//inactive tab
		if(isset($wpautbox_pro['tab_skin']) && $wpautbox_pro['tab_skin'] == 'custom'){
			if(isset($wpautbox_pro['inactive-color']) && !empty($wpautbox_pro['inactive-color']) &&'default' != $wpautbox_pro['inactive-color']){
				$style .= 'body .a-tabs>ul.a-tab-nav>li>a, body .a-tabs>ul.a-tab-nav>li>a:visited, body .a-tabs>ul.a-tab-nav>li>a:hover{ color: '. $wpautbox_pro['inactive-color'] .'; text-shadow: none;}';
			}
			if(isset($wpautbox_pro['inactive-bgcolor']) && !empty($wpautbox_pro['inactive-bgcolor']) &&'default' != $wpautbox_pro['inactive-bgcolor']){
				$style .= 'body .a-tabs>ul.a-tab-nav>li>a{ background: '. $wpautbox_pro['inactive-bgcolor'] .'; background-image: none;}';
			}
			if(isset($wpautbox_pro['inactive-bordercolor']) && !empty($wpautbox_pro['inactive-bordercolor']) &&'default' != $wpautbox_pro['inactive-bordercolor']){
				$style .= 'body .a-tabs>ul.a-tab-nav>li>a{ border-color: '. $wpautbox_pro['inactive-bordercolor'] .';}';
			}
		}

		//active tab
		if(isset($wpautbox_pro['tab_skin']) && $wpautbox_pro['tab_skin'] == 'custom'){
			if(isset($wpautbox_pro['active-color']) && !empty($wpautbox_pro['active-color']) && 'default' != $wpautbox_pro['active-color']){
				$style .= 'body .a-tabs>ul.a-tab-nav>li.a-tab-active a{ color: '. $wpautbox_pro['active-color'] .'; text-shadow: none;}';
			}
			if(isset($wpautbox_pro['active-bgcolor']) && !empty($wpautbox_pro['active-bgcolor']) &&'default' != $wpautbox_pro['active-bgcolor']){
				$style .= 'body .a-tabs>ul.a-tab-nav>li.a-tab-active a{ background: '. $wpautbox_pro['active-bgcolor'] .'; background-image: none;}';
			}
			if(isset($wpautbox_pro['active-bordercolor']) && !empty($wpautbox_pro['active-bordercolor']) &&'default' != $wpautbox_pro['active-bordercolor']){
				$style .= 'body .a-tabs>ul.a-tab-nav>li.a-tab-active a{ border-color: '. $wpautbox_pro['active-bordercolor'] .';}';
			}
		}
		

		//content tab
		if(isset($wpautbox_pro['tab_skin']) && $wpautbox_pro['tab_skin'] == 'custom'){
			if(isset($wpautbox_pro['tabcontent-color']) && !empty($wpautbox_pro['tabcontent-color']) &&'default' != $wpautbox_pro['tabcontent-color']){
				$style .= 'body .a-tabs .a-tab-container{ color: '. $wpautbox_pro['tabcontent-color'] .'; }';
			}
			if(isset($wpautbox_pro['tabcontent-bgcolor']) && !empty($wpautbox_pro['tabcontent-bgcolor']) &&'default' != $wpautbox_pro['tabcontent-bgcolor']){
				$style .= 'body .a-tabs .a-tab-container{ background: '. $wpautbox_pro['tabcontent-bgcolor'] .';}';
			}
			if(isset($wpautbox_pro['tabcontent-bordercolor']) && !empty($wpautbox_pro['tabcontent-bordercolor']) &&'default' != $wpautbox_pro['tabcontent-bordercolor']){
				$style .= 'body .a-tabs .a-tab-container{ border-color: '. $wpautbox_pro['tabcontent-bordercolor'] .';}';
			}
		}
			
		//content tab
		if(isset($wpautbox_pro['tabfooter-bordercolor']) && !empty($wpautbox_pro['tabfooter-bordercolor']) &&'default' != $wpautbox_pro['tabfooter-bordercolor']){
			$style .= 'body .a-tabs .wpautbox-socials{ border-color: '. $wpautbox_pro['tabfooter-bordercolor'] .'; }';
		}
		if(isset($wpautbox_pro['tabfooter-bgcolor']) && !empty($wpautbox_pro['tabfooter-bgcolor']) &&'default' != $wpautbox_pro['tabfooter-bgcolor']){
			$style .= 'body .a-tabs .wpautbox-socials{ background: '. $wpautbox_pro['tabfooter-bgcolor'] .';}';
		}

		//social icons
		if(isset($wpautbox_pro['social_custom_color']) && !empty($wpautbox_pro['social_custom_color'])){
			if(!empty($wpautbox_pro['social_custom_color']['regular']) && 'default' != $wpautbox_pro['social_custom_color']['regular']){
				$style .= '.a-tabs .wpautbox-socials-custom a .wpautbox-icon{ color: '. $wpautbox_pro['social_custom_color']['regular'] .';}';
			}
			if(!empty($wpautbox_pro['social_custom_color']['hover']) && 'default' != $wpautbox_pro['social_custom_color']['hover']){
				$style .= '.a-tabs .wpautbox-socials-custom a:hover .wpautbox-icon{ color: '. $wpautbox_pro['social_custom_color']['hover'] .';}';
			}
		}
		if(isset($wpautbox_pro['social_custom_bgcolor']) && !empty($wpautbox_pro['social_custom_bgcolor'])){
			if(!empty($wpautbox_pro['social_custom_bgcolor']['regular']) && 'default' != $wpautbox_pro['social_custom_bgcolor']['regular']){
				$style .= '.a-tabs .wpautbox-socials-custom a .wpautbox-icon{ background: '. $wpautbox_pro['social_custom_bgcolor']['regular'] .';}';
			}
			if(!empty($wpautbox_pro['social_custom_bgcolor']['hover']) && 'default' != $wpautbox_pro['social_custom_bgcolor']['hover']){
				$style .= '.a-tabs .wpautbox-socials-custom a:hover .wpautbox-icon{ background: '. $wpautbox_pro['social_custom_bgcolor']['hover'] .';}';
			}
		}
		if(isset($wpautbox_pro['custom-css']) && !empty($wpautbox_pro['custom-css'])){
			$style .= $wpautbox_pro['custom-css'];
		}

		$style .= '</style>';

		echo $style;
	}

	/**
	 * Add Co-Author Plus Support
	 *
	 * @since 2.0
	 */
	function wpauthbox_coauthor($position, $authorid, $postid){
		$return = ''; //initialize variable;
		if(function_exists('get_coauthors') && $this->settings['multi_author'] == '1'){
			$coauthors = get_coauthors($postid);
			$settings_role = $this->settings['user_levels'];
			if(($key = array_search('0', $settings_role)) !== false) {
			    unset($settings_role[$key]);
			}
			$settings_role = array_filter($settings_role);
			if(!empty($coauthors)){
				$count = 1;
				foreach ($coauthors as $coauthor) {
					if( isset($coauthor->roles) && is_array($coauthor->roles)){
						$user_role = array_shift($coauthor->roles);
					}else{
						$user_role = (isset($coauthor->roles)) ? $coauthor->roles : '';
					}
					if(array_key_exists( $user_role, $settings_role)){
						$return .= $this->construct_authorbox($position, $coauthor->ID);
					}
					if(isset($this->settings['co_author-limit']) && intval($this->settings['co_author-limit']) == $count){
						break;
					}
					$count++;
				}
			}
		}else{
			$return = $this->construct_authorbox($position, $authorid);
		}

		return $return;
	}

	/**
	 * Add Ajax
	 *
	 * @since 2.0
	 */
	function wpautbox_ajax(){
		$authorids = $_POST['authorids'];
		if(is_array($authorids)){
			foreach ($authorids as $authorid) {
				$daily = get_user_meta($authorid , 'wpautbox_views_' . strtotime( date('Y-m-d') ), true  );
				$daily = intval($daily) + 1;

				$week_number = ceil( date( 'j', strtotime( 'today' ) ) / 7 );
				$weekly = get_user_meta($authorid , 'wpautbox_views_' . strtotime( date('Y-m') ) . '_' . $week_number, true  );
				$weekly = intval($weekly) + 1;

				$monthly = get_user_meta($authorid , 'wpautbox_views_' . strtotime( date('Y-m') ) , true  );
				$monthly = intval($monthly) + 1;

				$yearly = get_user_meta($authorid , 'wpautbox_views_' . strtotime( date('Y') ) , true  );
				$yearly = intval($yearly) + 1;

				$overall = get_user_meta($authorid , 'wpautbox_views' , true  );
				$overall = intval($overall) + 1;

				//update user meta values
				update_user_meta( $authorid, 'wpautbox_views_' . strtotime( date('Y-m-d') ) , $daily );
				update_user_meta( $authorid, 'wpautbox_views_' . strtotime( date('Y-m') ) . '_' . $week_number , $weekly );
				update_user_meta( $authorid, 'wpautbox_views_' . strtotime( date('Y-m') ) , $monthly );
				update_user_meta( $authorid, 'wpautbox_views_' . strtotime( date('Y') ) , $yearly );
				update_user_meta( $authorid, 'wpautbox_views' , $overall );
			}
		}
		//update_user_meta( $user_id, 'wpautbox_user_fields', $wpautbox_meta );
		// print_r($authorids);
		die();
	}
	
}
$wpautbox_display = new wpautbox_display();
?>