<?php
/*##################################
	AUTHOR BOX WIDGET
################################## */

/**
 * Create widget
 *
 * @since 1.0
 */
class WPAUTBOX_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'wpautbox_widget', // Base ID
			__('WP Author Box', 'wpautbox'), // Name
			array( 'description' => __( 'Display Author Box to your sidebar.', 'wpautbox' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		global $wpautbox_pro;
		$authorid = isset($instance[ 'wpautbox_authorid' ]) ? $instance[ 'wpautbox_authorid' ] : '';
		$hide = array();
		if( isset($instance[ 'wpautbox_about_tab' ]) && 1 == $instance[ 'wpautbox_about_tab' ] ){
			$hide[] = 'about_tab';
		}
		if( isset($instance[ 'wpautbox_latest_tab' ]) && 1 == $instance[ 'wpautbox_latest_tab' ] ){
			$hide[] = 'latest_tab';
		}
		if (isset($wpautbox_pro['tabs']) && !empty($wpautbox_pro['tabs'])) {
			$tab_count = 0;
			foreach ($wpautbox_pro['tabs']['redux_group_data'] as $value) {
				if( isset($instance[ 'wpautbox_custom_tab_'. $tab_count  ]) && '1' == $instance[ 'wpautbox_custom_tab_'. $tab_count  ] ){
					$hide[] = 'custom_tab_' . $tab_count;
				}
				$tab_count++;
			}
		}
		
		echo $args['before_widget'];
		if ( ! empty( $authorid ) ){
			$sc = '[wpautbox authorid="'. $authorid .'"';
			if(!empty($hide)){
				$sc .=' hide="'. implode(',', $hide) .'"';
			}
			$sc .= ']';
			echo do_shortcode( $sc );
		}
		echo $args['after_widget'];
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		global $blog_id, $wpautbox_pro;
		$users = get_users('blog_id='. $blog_id .'&orderby=nicename');
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'wpautbox_authorid' ); ?>"><?php _e( 'Select User:' ); ?></label> 
		<select class="widefat" id="<?php echo $this->get_field_id( 'wpautbox_authorid' ); ?>" name="<?php echo $this->get_field_name( 'wpautbox_authorid' ); ?>">
			<option value=""><?php _e('Select User', 'wpautbox')?></option>
			<option value="-1" <?php if($instance[ 'wpautbox_authorid' ] == '-1'){ echo 'selected="selected"'; }?> ><?php _e('Current Post Author', 'wpautbox')?></option>
			<?php
			foreach ($users as $user) {
				$selected = "";
				if( $user->ID == $instance[ 'wpautbox_authorid' ] ){
					$selected = 'selected="selected"';
				}
		        echo '<option value="'. $user->ID .'" '. $selected .'>[' . $user->user_nicename . '] - '. get_the_author_meta( 'first_name', $user->ID ) .' '. get_the_author_meta( 'last_name', $user->ID ) .'</option>';
		    }
			?>
		</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'wpautbox_about_tab' ); ?>"><input type="checkbox" id="<?php echo $this->get_field_id( 'wpautbox_about_tab' ); ?>" name="<?php echo $this->get_field_name( 'wpautbox_about_tab' ); ?>" value="1" <?php echo ( isset($instance[ 'wpautbox_about_tab']) && 1 == $instance[ 'wpautbox_about_tab'] ) ? 'checked="checked"' : '';?>> 
				<?php _e( 'Hide About Tab' ); ?>
			</label> 
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'wpautbox_latest_tab' ); ?>"><input type="checkbox" id="<?php echo $this->get_field_id( 'wpautbox_latest_tab' ); ?>" name="<?php echo $this->get_field_name( 'wpautbox_latest_tab' ); ?>" value="1" <?php echo ( isset($instance[ 'wpautbox_latest_tab']) && 1 == $instance[ 'wpautbox_latest_tab'] ) ? 'checked="checked"' : '';?>> 
				<?php _e( 'Hide Latest Posts Tab' ); ?>
			</label> 
		</p>
		<?php
		if (isset($wpautbox_pro['tabs']) && !empty($wpautbox_pro['tabs'])) {
			$tab_count = 0;
			foreach ($wpautbox_pro['tabs']['redux_group_data'] as $key => $value) { ?>
				<p>
					<label for="<?php echo $this->get_field_id( 'wpautbox_custom_tab_'. $key ); ?>">
						<input type="checkbox" id="<?php echo $this->get_field_id( 'wpautbox_custom_tab_'. $key ); ?>" name="<?php echo $this->get_field_name( 'wpautbox_custom_tab_'. $key ); ?>" value="1" <?php echo ( isset($instance[ 'wpautbox_custom_tab_'. $key ]) && 1 == $instance[ 'wpautbox_custom_tab_'. $key ] ) ? 'checked="checked"' : '';?>> 
						<?php _e( 'Hide '. $wpautbox_pro['custom-tab-label'][ $tab_count ] .' Tab' ); ?>
					</label> 
				</p>
				<?php
				$tab_count++;
			}
		} 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		global $wpautbox_pro;
		$instance = array();
		$instance['wpautbox_authorid'] = ( ! empty( $new_instance['wpautbox_authorid'] ) ) ? strip_tags( $new_instance['wpautbox_authorid'] ) : '';
		$instance['wpautbox_about_tab'] = ( ! empty( $new_instance['wpautbox_about_tab'] ) ) ? strip_tags( $new_instance['wpautbox_about_tab'] ) : '';
		$instance['wpautbox_latest_tab'] = ( ! empty( $new_instance['wpautbox_latest_tab'] ) ) ? strip_tags( $new_instance['wpautbox_latest_tab'] ) : '';
		if (isset($wpautbox_pro['tabs']) && !empty($wpautbox_pro['tabs'])) {
			foreach ($wpautbox_pro['tabs']['redux_group_data'] as $key => $value) {
				$instance['wpautbox_custom_tab_' . $key] = ( ! empty( $new_instance['wpautbox_custom_tab_'. $key] ) ) ? strip_tags( $new_instance['wpautbox_custom_tab_'. $key] ) : '';
			}
		}

		return $instance;
	}
}

/**
 * Create Top Author Widget
 *
 * @since 2.0
 */
class WPAUTBOX_Widget_TOP extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'wpautbox_top_author', // Base ID
			__('Top Authors', 'wpautbox'), // Name
			array( 'description' => __( 'Display Top Authors by Number of Post or Views to your sidebar.', 'wpautbox' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		global $wpautbox_pro, $wpdb;
		$arg = array(
				'number'	=> intval($instance['wpautbox_top_total']),
				'order'		=> 'DESC'
			);
		switch ($instance['wpautbox_top_orderby']) {
			case 'posts':
				$arg['orderby'] = 'post_count';
				$authors = get_users( $arg );
				break;

			case 'daily_views':
				$meta_key = 'wpautbox_views_' . strtotime( date('Y-m-d') );
				$query = "SELECT meta_table.user_id, meta_table.meta_value
				FROM $wpdb->usermeta meta_table
				WHERE meta_table.meta_key = '". $meta_key ."'
				ORDER BY CAST(meta_table.meta_value AS SIGNED) DESC
				LIMIT 0, " . intval($instance['wpautbox_top_total']);

				$authors = $wpdb->get_col($query);
				break;

			case 'weekly_views':
				$week_number = ceil( date( 'j', strtotime( 'today' ) ) / 7 );
				$meta_key = 'wpautbox_views_' . strtotime( date('Y-m') ) . '_' . $week_number;
				$query = "SELECT meta_table.user_id, meta_table.meta_value
				FROM $wpdb->usermeta meta_table
				WHERE meta_table.meta_key = '". $meta_key ."'
				ORDER BY CAST(meta_table.meta_value AS SIGNED) DESC
				LIMIT 0, " . intval($instance['wpautbox_top_total']);

				$authors = $wpdb->get_col($query);
				break;

			case 'monthly_views':
				$meta_key = 'wpautbox_views_' . strtotime( date('Y-m') );
				$query = "SELECT meta_table.user_id, meta_table.meta_value
				FROM $wpdb->usermeta meta_table
				WHERE meta_table.meta_key = '". $meta_key ."'
				ORDER BY CAST(meta_table.meta_value AS SIGNED) DESC
				LIMIT 0, " . intval($instance['wpautbox_top_total']);

				$authors = $wpdb->get_col($query);
				break;

			case 'yearly_views':
				$meta_key = 'wpautbox_views_' . strtotime( date('Y') );
				$query = "SELECT meta_table.user_id, meta_table.meta_value
				FROM $wpdb->usermeta meta_table
				WHERE meta_table.meta_key = '". $meta_key ."'
				ORDER BY CAST(meta_table.meta_value AS SIGNED) DESC
				LIMIT 0, " . intval($instance['wpautbox_top_total']);

				$authors = $wpdb->get_col($query);
				break;

			case 'total_views':
				$meta_key = 'wpautbox_views';
				$query = "SELECT meta_table.user_id, meta_table.meta_value
				FROM $wpdb->usermeta meta_table
				WHERE meta_table.meta_key = '". $meta_key ."'
				ORDER BY CAST(meta_table.meta_value AS SIGNED) DESC
				LIMIT 0, " . intval($instance['wpautbox_top_total']);

				$authors = $wpdb->get_col($query);
				break;
			
			default:
				# code...
				break;
		}
		
		echo $args['before_widget'];
			$html = '<div class="wpautbox-top-widget">';
			if ( ! empty( $instance['wpautbox_top_title'] ) ) {
				$html .= $args['before_title'] . apply_filters( 'widget_title', $instance['wpautbox_top_title'] ) . $args['after_title'];
			}
			$html .= '<div class="wpautbox-top-widget-content">';
			if(is_array($authors) && !empty($authors)){
				$counter = 1;
				foreach ($authors as $author) {
					if(isset($author->ID)){
						$author_ID = $author->ID;
					}else{
						$author_ID = $author;
					}
					if(false == get_user_by( 'id', $author_ID )){
						//invalid authors
					}else{
						$post_count = count_user_posts($author_ID);
						if(isset($meta_key)){
							// $html .= $meta_key;
							$views = get_user_meta($author_ID , $meta_key, true  );
						}
						
						$wpautbox_meta = get_user_meta( $author_ID, 'wpautbox_user_fields', false );
						if(isset($wpautbox_meta[0])){
							$wpautbox_meta = unserialize( base64_decode($wpautbox_meta[0]) );
						}
						$html .= '<div class="wpautbox-top-author">';

						if(isset($wpautbox_meta['user']['image']) && !empty($wpautbox_meta['user']['image'])){
							$image_attributes = wp_get_attachment_image_src( $wpautbox_meta['user']['image'], array(90,90) );
							$html .= '<a href="'. get_author_posts_url($author_ID) .'"><img src="'. $image_attributes[0] .'" class="avatar" /></a>';
						}else{
							$html .= '<a href="'. get_author_posts_url($author_ID) .'">'. get_avatar( $author_ID, 90 ) . '</a>';
						}
						$html .= '<h4><a href="'. get_author_posts_url($author_ID) .'">'. get_the_author_meta( 'display_name',$author_ID ) .'</a></h4>';
						if('posts' == $instance['wpautbox_top_orderby']){
							$html .= '<div class="wpautbox-ranking"><span class="wpautbox-icon wpautbox-icon-doc-text">'. $post_count . ' ' . (isset($wpautbox_pro['widget_top_post-label']) ? $wpautbox_pro['widget_top_post-label'] : '') .'</span></div>';
						}else{
							$html .= '<div class="wpautbox-ranking"><span class="wpautbox-icon wpautbox-icon-eye">'. $views . ' ' . (isset($wpautbox_pro['widget_top_view-label']) ? $wpautbox_pro['widget_top_view-label'] : '') .'</span></div>';
						}
						
						$html .= '<div style="clear:both;"></div>';
						$html .= '</div>';
						$counter++;
					}
					// print_r($author);break;
				}
			}
			$html .= '</div>';
			echo $html .= '</div>';
		echo $args['after_widget'];
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		global $blog_id, $wpautbox_pro;
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'wpautbox_top_title' ); ?>"><?php _e( 'Title:', 'wpautbox' ); ?></label> 
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'wpautbox_top_title' ); ?>" name="<?php echo $this->get_field_name( 'wpautbox_top_title' ); ?>" value="<?php echo (isset($instance[ 'wpautbox_top_title'])) ? $instance[ 'wpautbox_top_title'] : '';?>"> 
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'wpautbox_top_orderby' ); ?>"><?php _e( 'Order by:', 'wpautbox' ); ?></label> 
		<select class="widefat" id="<?php echo $this->get_field_id( 'wpautbox_top_orderby' ); ?>" name="<?php echo $this->get_field_name( 'wpautbox_top_orderby' ); ?>">
			<option value="posts" <?php if(isset($instance[ 'wpautbox_top_orderby']) && $instance[ 'wpautbox_top_orderby'] == 'posts' ){ echo 'selected="selected"'; }?>><?php _e('Number of Posts', 'wpautbox')?></option>
			<option value="daily_views" <?php if(isset($instance[ 'wpautbox_top_orderby']) && $instance[ 'wpautbox_top_orderby'] == 'daily_views' ){ echo 'selected="selected"'; }?>><?php _e('Daily Author Box Views')?></option>
			<option value="weekly_views" <?php if(isset($instance[ 'wpautbox_top_orderby']) && $instance[ 'wpautbox_top_orderby'] == 'weekly_views' ){ echo 'selected="selected"'; }?>><?php _e('Weekly Author Box Views')?></option>
			<option value="monthly_views" <?php if(isset($instance[ 'wpautbox_top_orderby']) && $instance[ 'wpautbox_top_orderby'] == 'monthly_views' ){ echo 'selected="selected"'; }?>><?php _e('Monthly Author Box Views')?></option>
			<option value="yearly_views" <?php if(isset($instance[ 'wpautbox_top_orderby']) && $instance[ 'wpautbox_top_orderby'] == 'yearly_views' ){ echo 'selected="selected"'; }?>><?php _e('This Years\'s Author Box Views')?></option>
			<option value="total_views" <?php if(isset($instance[ 'wpautbox_top_orderby']) && $instance[ 'wpautbox_top_orderby'] == 'total_views' ){ echo 'selected="selected"'; }?>><?php _e('Total Author Box Views')?></option>
		</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'wpautbox_top_total' ); ?>"><?php _e( 'Total Number of Authors:', 'wpautbox' ); ?></label> 
			<input type="text" size="3" id="<?php echo $this->get_field_id( 'wpautbox_top_total' ); ?>" name="<?php echo $this->get_field_name( 'wpautbox_top_total' ); ?>" value="<?php echo ( isset($instance[ 'wpautbox_top_total']) && !empty($instance[ 'wpautbox_top_total'])) ? $instance[ 'wpautbox_top_total'] : '5';?>" > 
		</p>
		<?php
		
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		global $wpautbox_pro;
		$instance = array();
		$instance['wpautbox_top_title'] = ( ! empty( $new_instance['wpautbox_top_title'] ) ) ? strip_tags( $new_instance['wpautbox_top_title'] ) : '';
		$instance['wpautbox_top_orderby'] = ( ! empty( $new_instance['wpautbox_top_orderby'] ) ) ? strip_tags( $new_instance['wpautbox_top_orderby'] ) : '';
		$total = ( ! empty( $new_instance['wpautbox_top_total'] ) ) ? strip_tags( $new_instance['wpautbox_top_total'] ) : '';
		$instance['wpautbox_top_total'] = intval($total);
		return $instance;
	}
}

// register WPAUTBOX_Widget widget
function register_wpautbox_widget() {
    register_widget( 'WPAUTBOX_Widget' );
    register_widget( 'WPAUTBOX_Widget_TOP' );
}
add_action( 'widgets_init', 'register_wpautbox_widget' );
?>