<?php
/*
 * Add Gravity Form Tab Support
 * @version 2.0
 */

/**
 * Add New Button Field
 *
 * @since 2.0
 */
add_filter( 'gform_add_field_buttons', 'wpautbox_gform_add_field_buttons' );
function wpautbox_gform_add_field_buttons($field_groups){
	foreach( $field_groups as &$group ){
		if( $group["name"] == "post_fields" ){ // to add to the Advanced Fields
			//if( $group["name"] == "standard_fields" ){ // to add to the Standard Fields
			//if( $group["name"] == "post_fields" ){ // to add to the Standard Fields
			$group["fields"][] = array(
				"label" => __("WP Author Box Email", "gravityforms"),
				"class"=>"button",
				"value" => __("Author Box Email", "gravityforms"),
				"onclick" => "StartAddField('wpautboxemail');"
			);

			$group["fields"][] = array(
				"label" => __("WP Author Box Name", "gravityforms"),
				"class"=>"button",
				"value" => __("Author Box Name", "gravityforms"),
				"onclick" => "StartAddField('wpautboxname');"
			);
			break;
		}

	}
	return $field_groups;
}

/**
 * Add Title to Custom Field
 *
 * @since 2.0
 */
add_filter( 'gform_field_type_title' , 'wpautbox_gform_field_type_title' );
function wpautbox_gform_field_type_title( $type ) {
	if ( $type == 'wpautboxemail' ){
		return __( 'WP Author Box Email' , "gravityforms" );
	}

	if ( $type == 'wpautboxname' ){
		return __( 'WP Author Box Name' , "gravityforms" );
	}
}

// Now we execute some javascript technicalitites for the field to load correctly
add_action( "gform_editor_js", "wpautbox_gform_editor_js" );
function wpautbox_gform_editor_js(){
?>
	<script type='text/javascript'>
		jQuery(document).ready(function($) {
			$(".gform_wpautboxemail > input").hide();
			$(".gform_wpautboxname > input").hide();
			//Add all textarea settings to the "TOS" field plus custom "tos_setting"
			// fieldSettings["tos"] = fieldSettings["textarea"] + ", .tos_setting"; // this will show all fields that Paragraph Text field shows plus my custom setting
			// from forms.js; can add custom "tos_setting" as well
			fieldSettings["wpautboxemail"] = ".label_setting"; //this will show all the fields of the Paragraph Text field minus a couple that I didn't want to appear.
			fieldSettings["wpautboxname"] = ".label_setting"; //this will show all the fields of the Paragraph Text field minus a couple that I didn't want to appear.
			//binding to the load field settings event to initialize the checkbox
			$(document).bind("gform_load_field_settings", function(event, field, form){
				$(".gform_wpautboxemail > input").hide();
				$(".gform_wpautboxname > input").hide();
			});
		});
	</script>
<?php
}

add_filter("gform_field_input", "wpautbox_map_input", 10, 5);
function wpautbox_map_input($input, $field, $value, $lead_id, $form_id){
    if($field["type"] == "wpautboxemail"){
        $input = '<input type="hidden" id="input_'. $form_id .'_'. $field['id'] .'" name="input_'. $field['id'] .'" value="{{wpauthorbox_email}}">';
    }
    if($field["type"] == "wpautboxname"){
        $input = '<input type="hidden" id="input_'. $form_id .'_'. $field['id'] .'" name="input_'. $field['id'] .'" value="{{wpauthorbox_name}}">';
    }
    return $input;
}

// Add a custom class to the field li
add_action("gform_field_css_class", "wpautbox_custom_class", 10, 3);
function wpautbox_custom_class($classes, $field, $form){
	if( $field["type"] == "wpautboxemail" ){
		$classes .= " gform_hidden gform_wpautboxemail";
	}
	if( $field["type"] == "wpautboxname" ){
		$classes .= " gform_hidden gform_wpautboxname";
	}
	return $classes;
}

add_filter("gform_email_fields_notification_admin", 'wpautbox_add_field_to_email_list', 10, 2);
function wpautbox_add_field_to_email_list($field_list, $form){

   //Adds field with ID=1 to the list of email fields
   foreach($form["fields"] as $field){
       if($field["type"] == "wpautboxemail"){
       		$field_list[] = $field;
       }
   }

   return $field_list;
}
?>