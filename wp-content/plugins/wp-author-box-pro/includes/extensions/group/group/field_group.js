/* global redux_change */

/*global redux_change, redux*/

(function( $ ) {
    "use strict";

    redux.field_objects = redux.field_objects || {};
    redux.field_objects.group = redux.field_objects.group || {};

    $( document ).ready(
        function() {
            redux.field_objects.group.init();
        }
    );

    redux.field_objects.group.sort_groups = function( selector ) {
        if ( !selector.hasClass( 'redux-container-group' ) ) {
            selector = selector.parents( '.redux-container-group:first' );
        }

        selector.find( '.redux-group-accordion-group' ).each(
            function( idx ) {

                var id = $( this ).attr( 'data-sortid' );
                var input = $( this ).find( "input[name*='[" + id + "]']" );
                input.each(
                    function() {
                        $( this ).attr( 'name', $( this ).attr( 'name' ).replace( '[' + id + ']', '[' + idx + ']' ) );
                    }
                );

                var select = $( this ).find( "select[name*='[" + id + "]']" );
                select.each(
                    function() {
                        $( this ).attr( 'name', $( this ).attr( 'name' ).replace( '[' + id + ']', '[' + idx + ']' ) );
                    }
                );
                $( this ).attr( 'data-sortid', idx );

                // Fix the accordian header
                var header = $( this ).find( '.ui-accordion-header' );
                var split = header.attr( 'id' ).split( '-header-' );
                header.attr( 'id', split[0] + '-header-' + idx );
                split = header.attr( 'aria-controls' ).split( '-panel-' );
                header.attr( 'aria-controls', split[0] + '-panel-' + idx );

                // Fix the accordian content
                var content = $( this ).find( '.ui-accordion-content' );
                var split = content.attr( 'id' ).split( '-panel-' );
                content.attr( 'id', split[0] + '-panel-' + idx );
                split = content.attr( 'aria-labelledby' ).split( '-header-' );
                content.attr( 'aria-labelledby', split[0] + '-header-' + idx );

                // Update the slide-title key
                $( this ).find( '.slide-title' ).attr( 'data-key', idx );

            }
        );
    };

    redux.field_objects.group.init = function( selector ) {

        if ( !selector ) {
            selector = $( document ).find( '.redux-container-group' );
        }

        $( selector ).each(
            function() {

                var el = $( this );
                var parent = el;

                if ( !el.hasClass( 'redux-field-container' ) ) {
                    parent = el.parents( '.redux-field-container:first' );
                }

                var gid = parent.attr( 'data-id' );

                var blank = el.find('.redux-group-accordion-group:last-child');
                redux.group[gid].blank = blank.clone().wrap('<p>').parent().html();


                if ( parent.hasClass( 'redux-container-group' ) ) {
                    parent.addClass( 'redux-field-init' );
                }

                if ( parent.hasClass( 'redux-field-init' ) ) {
                    parent.removeClass( 'redux-field-init' );
                } else {
                    return;
                }

                var active = false;

                if ( el.find( '.slide-title' ).length < 2 ) {
                    active = 0;
                }

                el.find( ".redux-group-accordion" ).accordion(
                    {
                        header: "> div > fieldset > h3",
                        collapsible: true,
                        active: active,
                        activate: function( event, ui ) {
                            $.redux.initFields();
                        },
                        heightStyle: "content",
                        icons: {
                            "header": "ui-icon-plus",
                            "activeHeader": "ui-icon-minus"
                        }
                    }
                ).sortable(
                    {
                        axis: "y",
                        handle: "h3",
                        connectWith: ".redux-group-accordion",
                        placeholder: "ui-state-highlight",
                        start: function( e, ui ) {
                            ui.placeholder.height( ui.item.height() );
                            ui.placeholder.width( ui.item.width() );
                        },
                        stop: function( event, ui ) {
                            // IE doesn't register the blur when sorting
                            // so trigger focusout handlers to remove .ui-state-focus
                            ui.item.children( "h3" ).triggerHandler( "focusout" );

                            redux.field_objects.group.sort_groups( $( this ) );

                        }
                    }
                );

                el.find( '.redux-group-accordion-group input[data-title="true"]' ).on(
                    'keyup', function( event ) {
                        $( this ).closest( '.redux-group-accordion-group' ).find( '.redux-group-header' ).text( event.target.value );
                        $( this ).closest( '.redux-group-accordion-group' ).find( '.slide-title' ).val( event.target.value );
                    }
                );

                el.find( "i.group_field_edit_icon, span.redux-group-header" ).on(
                    "click", function( e ) {
                        e.stopPropagation();
                        e.preventDefault();
                        var answer = prompt(
                            "Enter the new title:", $( this ).parent().find( "span.redux-group-header" ).text()
                        );
                        if ( answer !== null ) {
                            $( this ).parent().find( "span.redux-group-header" ).text( answer );
                            $( this ).parent().parent().find( "input.slide-title" ).val( answer );
                        }
                    }
                );

                // Handler to remove the given group
                el.find( '.redux-groups-remove' ).live(
                    'click', function() {
                        redux_change( $( this ) );
                        var parent = $( this ).parents( '.redux-container-group:first' );
                        var gid = parent.attr( 'data-id' );
                        redux.group[gid].blank = $( this ).parents( '.redux-group-accordion-group:first' ).clone( true, true );
                        $( this ).parents( '.redux-group-accordion-group:first' ).slideUp(
                            'medium', function() {
                                $( this ).remove();
                                redux.field_objects.group.sort_groups( el );
                            }
                        );
                    }
                );


                String.prototype.reduxReplaceAll = function( s1, s2 ) {
                    return this.replace(
                        new RegExp( s1.replace( /[.^$*+?()[{\|]/g, '\\$&' ), 'g' ),
                        s2
                    );
                };



                el.find( '.redux-groups-add' ).click(
                    function() {

                        var parent = $( this ).parent().find( '.redux-group-accordion:first' );
                        var id = parent.find( '.redux-group-accordion-group' ).size(); // Index number
                        var gid = parent.attr( 'data-id' ); // Group id

                        if ( parent.find( '.redux-group-accordion-group:last' ).find( '.ui-accordion-header' ).hasClass( 'ui-state-active' ) ) {
                            parent.find( '.redux-group-accordion-group:last' ).find( '.ui-accordion-header' ).click();
                        }

                        var newSlide = parent.find( '.redux-group-accordion-group:last' ).clone( true, true );

                        if (newSlide.length == 0) {
                            newSlide = redux.group[gid].blank;
                        }

                        if ( redux.group[gid] ) {
                            redux.group[gid].count = el.find( 'input.slide-title' ).length;
                            var html = redux.group[gid].html.reduxReplaceAll( '99999', id );
                            var slideTitle = newSlide.find( '.slide-title' );
                            var oldval = '['+slideTitle.attr('data-key' )+']';
                            slideTitle.attr(
                                'name', slideTitle.attr('name').replace( oldval, '['+id+']' )
                            ).attr( 'data-key', id );
                            $( newSlide ).find( 'input.slide-title' ).val( redux.group[gid].title );
                            $( newSlide ).find( 'h3' ).text( '' ).append( '<span class="redux-group-header">' + redux.group[gid].title + '</span><span class="ui-accordion-header-icon ui-icon ui-icon-plus"></span><i class="group_field_edit_icon el-icon-pencil"></i>' );
                        }

                        newSlide.find( '.redux-group-accordion-group input[data-title="true"]' ).on(
                            'keyup', function( event ) {
                                $( this ).closest( '.redux-group-accordion-group' ).find( '.redux-group-header' ).text( event.target.value );
                                $( this ).closest( '.redux-group-accordion-group' ).find( '.slide-title' ).val( event.target.value );
                            }
                        );

                        newSlide.find( "i.group_field_edit_icon" ).on(
                            "click", function( e ) {
                                e.stopPropagation();
                                e.preventDefault();
                                var answer = prompt(
                                    "Enter the new title:", $( this ).parent().find( "span.redux-group-header" ).text()
                                );
                                if ( answer !== null ) {
                                    $( this ).parent().find( "span.redux-group-header" ).text( answer );
                                    $( this ).parent().parent().find( "input.slide-title" ).val( answer );
                                }
                            }
                        );

                        newSlide.find( '.ui-accordion-content' ).html( html );
                        // Append to the accordian
                        $( parent ).append( newSlide );
                        // Reorder
                        redux.field_objects.group.sort_groups( newSlide );
                        // Refresh the JS object
                        var newSlide = $( this ).parent().find( '.redux-group-accordion:first' );
                        newSlide.find( '.redux-group-accordion-group:last .ui-accordion-header' ).click();

                    }
                );
            }
        );
    };
})( jQuery );
