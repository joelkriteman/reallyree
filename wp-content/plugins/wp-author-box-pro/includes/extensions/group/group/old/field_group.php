<?php
/**
 * Redux Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Redux Framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Redux Framework. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     Redux Framework
 * @subpackage  Group
 * @author      Kevin Provance (kprovance)
 * @version     1.0.1
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

// Don't duplicate me!
if( !class_exists( 'ReduxFramework_group' ) ) {

    /**
     * Main ReduxFramework_css_layout class
     *
     * @since       1.0.0
     */
    class ReduxFramework_group {

      /**
       * Class Constructor. Defines the args for the extions class
       *
       * @since       1.0.0
       * @access      public
       * @param       array $field Field sections.
       * @param       array $value Values.
       * @param       array $parent Parent object.
       * @return      void
       */
        public function __construct( $field = array(), $value ='', $parent ) {

            // Set required variables
            $this->parent   = $parent;
            $this->field    = $field;
            $this->value    = $value;

            // Set extension dir & url
            if ( empty( $this->extension_dir ) ) {
                $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
                $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
            }
        }

        /**
         * Field Render Function.
         *
         * Takes the vars and outputs the HTML for the field in the settings
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function render() {
            if ( ! isset( $this->field['groupname'] ) ) {
                $this->field['groupname'] = "";
            }
            
            if (empty($this->value) || !is_array($this->value)) {
                
                $this->value = array(
                    array(
                        'slide_title' => __('New', 'redux-framework').' '.$this->field['groupname'],
                        'slide_sort' => '0',
                    )
                );
            }
            
            if ( isset( $this->field['subfields'] ) && empty( $this->field['fields'] ) ) {
                 $this->field['fields'] = $this->field['subfields'];
                 unset( $this->field['subfields'] );
            }
            
            $groups = $this->value;

            echo '<div class="redux-group">';
            echo '<input type="hidden" class="redux-dummy-slide-count" id="redux-dummy-' . $this->field['id'] . '-count" name="redux-dummy-' . $this->field['id'] . '-count" value="' . count($groups) . '" />';
            echo '<div id="redux-groups-accordion">';

            // Create dummy content for the adding new ones
            echo '<div class="redux-groups-accordion-group redux-dummy" style="display:none" id="redux-dummy-' . $this->field['id'] . '"><h3><span class="redux-groups-header">' . __("New ", "redux-framework") . $this->field['groupname'] . '</span></h3>';
            echo '<div>';//according content open
                
            echo '<table style="margin-top: 0;" class="redux-groups-accordion redux-group form-table no-border">';    
            echo '<fieldset><input type="hidden" id="' . $this->field['id'] . '_slide-title" data-name="' . $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][@][slide_title]" value="" class="regular-text slide-title" /></fieldset>';
            echo '<input type="hidden" class="slide-sort" data-name="' . $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][@][slide_sort]" id="' . $this->field['id'] . '-slide_sort" value="" />';
            $field_is_title = true;
                foreach ($this->field['fields'] as $field) {
                    //we will enqueue all CSS/JS for sub fields if it wasn't enqueued
                    $this->enqueue_dependencies($field['type']);

                    echo '<tr><td>';
                    
                    if(isset($field['class'])) {
                        $field['class'] .= " group";
                    } else {
                        $field['class'] = " group";
                    }

                    if (!empty($field['title'])) {
                        echo '<h4>' . $field['title'] . '</h4>';
                    }
                    
                    if (!empty($field['subtitle'])) {
                        echo '<span class="description">' . $field['subtitle'] . '</span>';
                    }

                    $origFieldID = $field['id'];
                    
                    //$field['id'] = $field['id'] . '-@' ;
                    //$field["name"] = $this->parent->args['opt_name'] . "[" . $this->field[ 'id' ] . "][@][" . $origFieldID . "]";
                    
                    
                    $value = empty($this->parent->options[$field['id']][0]) ? "" : $this->parent->options[$field['id']][0];
                    //logConsole('field',$field);
                    //die;
                    ob_start();
                    $this->parent->_field_input($field, $value);

                    $content = ob_get_contents();
//logConsole('content',$content);
//die;
                    //adding sorting number to the name of each fields in group
                    $name = $this->parent->args['opt_name'] . '[' . $field['id'] . ']';
                    $content = str_replace($name,$this->parent->args['opt_name'] . '[' . $this->field['id'] . '][@]['.$field['id'].']', $content);
                    // remove the name property. asigned by the controller, create new data-name property for js
                    $content = str_replace('name', 'data-name', $content);

                    if(($field['type'] === "text") && ($field_is_title)) {
                        $content = str_replace('>', 'data-title="true" />', $content);
                        $field_is_title = false;
                    }

                    //we should add $sort to id to fix problem with select field
                    $content = str_replace(' id="'.$field['id'].'-select"', ' id="'.$field['id'].'-select-'.$sort.'"', $content);
                    
                    $_field = apply_filters('redux-support-group',$content, $field, 0);
                    ob_end_clean();
                    echo $_field;
                    
                    echo '</td></tr>';
                }
                echo '</table>';
                echo '<a href="javascript:void(0);" class="button deletion redux-groups-remove">' . __('Delete', 'redux-framework').' '. $this->field['groupname']. '</a>';
                echo '</div></div>';

            // Create real groups
            $x = 0;
            foreach ($groups as $group) {

                echo '<div class="redux-groups-accordion-group"><h3><span class="redux-groups-header">' . $group['slide_title'] . '</span></h3>';
                echo '<div>';//according content open
                
                echo '<table style="margin-top: 0;" class="redux-groups-accordion redux-group form-table no-border">';
                
                //echo '<h4>' . __('Group Title', 'redux-framework') . '</h4>';
                echo '<fieldset><input type="hidden" id="' . $this->field['id'] . '-slide_title_' . $x . '" name="' . $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][' . $x . '][slide_title]" value="' . esc_attr($group['slide_title']) . '" class="regular-text slide-title" /></fieldset>';
                echo '<input type="hidden" class="slide-sort" name="' . $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][' . $x . '][slide_sort]" id="' . $this->field['id'] . '-slide_sort_' . $x . '" value="' . $group['slide_sort'] . '" />';
                
                $field_is_title = true;

                foreach ($this->field['fields'] as $field) {
                    //we will enqueue all CSS/JS for sub fields if it wasn't enqueued
                    $this->enqueue_dependencies($field['type']);
                    
                    echo '<tr><td>';
                    
                    if(isset($field['class'])) {
                        $field['class'] .= " group";
                    } else {
                        $field['class'] = " group";
                    }

                    if (!empty($field['title'])) {
                        echo '<h4>' . $field['title'] . '</h4>';
                    }
                    
                    if (!empty($field['subtitle'])) {
                        echo '<span class="description">' . $field['subtitle'] . '</span>';
                    }
                    
                    if (isset($group[$field['id']]) && !empty($group[$field['id']])) {
                    	$value = $group[$field['id']];   	
                    }
                    
                    //echo $x;
                    $origFieldID = $field['id'];
                    
                    $field['id'] = $field['id'] . '-' . $x ;
                    $field["name"] = $this->parent->args['opt_name'] . "[" . $this->field[ 'id' ] . "][" . $x . "][" . $origFieldID . "]";
                    
                    $value = empty($value) ? "" : $value;

                    ob_start();
                    $this->parent->_field_input($field, $value);
                    //if (isset($this->options[$field['id']]) && !empty($this->options[$field['id']]) && is_array($this->options[$field['id']])) {
	                //    $value = next($this->options[$field['id']]);
                    //}

                    $content = ob_get_contents();

                    //adding sorting number to the name of each fields in group
                    //$field['id'] = $origFieldID;
                    //$name = $this->parent->args['opt_name'] . '[' . $field['id'] . ']';
                    //$content = str_replace($name, $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][' . $x . '][' . $field['id'] . ']', $content);

                    //we should add $sort to id to fix problem with select field
                    $content = str_replace(' id="'.$field['id'].'-select"', ' id="'.$field['id'].'-select-'.$sort.'"', $content);

                    if(($field['type'] === "text") && ($field_is_title)) {
                        $content = str_replace('>', 'data-title="true" />', $content);
                        $field_is_title = false;
                    }
                    
                    $_field = apply_filters('redux-support-group',$content, $field, $x);
                    ob_end_clean();
                    echo $_field;
                    
                    echo '</td></tr>';
                }
                echo '</table>';
                echo '<a href="javascript:void(0);" class="button deletion redux-groups-remove">' . __('Delete', 'redux-framework').' '.$this->field['groupname']. '</a>';
                echo '</div></div>';
                $x++;
            }

            echo '</div><a href="javascript:void(0);" class="button redux-groups-add button-primary" rel-id="' . $this->field['id'] . '-ul" rel-name="' . $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][slide_title][]">' . __('Add', 'redux-framework') .' '.$this->field['groupname']. '</a><br/>';

            echo '</div>';

        }

        /**
         * Enqueue Function.
         *
         * If this field requires any scripts, or css define this function and register/enqueue the scripts/css
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function enqueue() {

            $extension = ReduxFramework_extension_css_layout::getInstance();

            // Set up min files for dev_mode = false.
            $min = Redux_Functions::isMin();

//            wp_enqueue_script(
//                'redux-sheep-it-js',
//                $this->extension_url . 'vendor/jquery.sheepItPlugin' . $min . '.js',
//                array( 'jquery'),
//                time(),
//                true
//            );

            wp_enqueue_script(
                'redux-field-group-js',
                $this->extension_url . 'field_group' . $min . '.js',
                array('jquery', 'jquery-ui-core', 'jquery-ui-accordion', 'wp-color-picker'),
                time(),
                true
            );

            wp_enqueue_style(
                'redux-field-group-css',
                $this->extension_url . 'field_group.css',
                time(),
                true
            );
        }

        private function enqueue_dependencies( $field_type ) {
            $field_class = 'ReduxFramework_' . $field_type;

            if (!class_exists($field_class)) {
                $class_file = apply_filters('redux-typeclass-load', ReduxFramework::$_dir . 'inc/fields/' . $field_type . '/field_' . $field_type . '.php', $field_class);

                if ($class_file) {
                    /** @noinspection PhpIncludeInspection */
                    require_once( $class_file );
                }
            }

            if (class_exists($field_class) && method_exists($field_class, 'enqueue')) {
                $enqueue = new $field_class('', '', $this);
                $enqueue->enqueue();
            }
        }
    }
}