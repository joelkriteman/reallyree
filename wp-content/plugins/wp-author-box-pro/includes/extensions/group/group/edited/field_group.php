<?php
    /**
     * Redux Framework is free software: you can redistribute it and/or modify
     * it under the terms of the GNU General Public License as published by
     * the Free Software Foundation, either version 2 of the License, or
     * any later version.
     * Redux Framework is distributed in the hope that it will be useful,
     * but WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     * GNU General Public License for more details.
     * You should have received a copy of the GNU General Public License
     * along with Redux Framework. If not, see <http://www.gnu.org/licenses/>.
     *
     * @package     Redux Framework
     * @subpackage  Group
     * @author      Kevin Provance (kprovance)
     * @author      Dovy Paukstys (dovy)
     * @version     1.0.1
     */

// Exit if accessed directly
    if ( ! defined( 'ABSPATH' ) ) {
        exit;
    }

// Don't duplicate me!
    if ( ! class_exists( 'ReduxFramework_group' ) ) {

        /**
         * Main ReduxFramework_css_layout class
         *
         * @since       1.0.0
         */
        class ReduxFramework_group {

            /**
             * Class Constructor. Defines the args for the extions class
             *
             * @since       1.0.0
             * @access      public
             *
             * @param       array $field  Field sections.
             * @param       array $value  Values.
             * @param       array $parent Parent object.
             *
             * @return      void
             */
            public function __construct( $field = array(), $value = '', $parent ) {

                // Set required variables
                $this->parent       = $parent;
                $this->field        = $field;
                $this->value        = $value;
                $this->group_values = "";

                // Set extension dir & url
                if ( empty( $this->extension_dir ) ) {
                    $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
                    $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
                }
            }

            /**
             * Field Render Function.
             * Takes the vars and outputs the HTML for the field in the settings
             *
             * @since       1.0.0
             * @access      public
             * @return      void
             */
            public function render() {

                //print_r( $this->value );

                if ( ! isset( $this->field['groupname'] ) ) {
                    $this->field['groupname'] = "";
                }

                if ( isset( $this->field['group_values'] ) && $this->field['group_values'] ) {
                    $this->group_values = '[' . $this->field['id'] . ']';
                }
                $title = trim( __( 'New', 'redux-framework' ) . ' ' . $this->field['groupname'] );
                if ( empty( $this->value ) || ! is_array( $this->value ) ) {

                    $this->value = array(
                        'redux_group_data' => array(
                            array(
                                'title' => $title
                            )
                        )
                    );


                }

                if ( isset( $this->field['subfields'] ) && empty( $this->field['fields'] ) ) {
                    $this->field['fields'] = $this->field['subfields'];
                    unset( $this->field['subfields'] );
                }

                echo '<div class="redux-group-accordion" data-id="' . $this->field['id'] . '" data-new-content-title="' . $title . ' ' . $this->field['groupname'] . ' ">';

                $x = 0;
                if ( isset ( $this->value['redux_group_data'] ) && is_array( $this->value['redux_group_data'] ) && ! empty ( $this->value['redux_group_data'] ) ) {
                    $groups = $this->value['redux_group_data'];

                    foreach ( $groups as $group ) {
                        if ( empty ( $group ) ) {
                            continue;
                        }

                        echo '<div class="redux-group-accordion-group" data-sortid="' . $x . '">';
                        echo '<table style="margin-top: 0;" class="redux-group-accordion redux-group form-table no-border">';
                        echo '<fieldset class="redux-field" data-id="' . $this->field['id'] . '">';

                        echo '<input type="hidden" name="' . $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][redux_group_data][' . $x . '][title]" value="' . esc_attr( $group['title'] ) . '" class="regular-text slide-title" data-key="' . $x . '" />';

                        echo '<h3><span class="redux-group-header">' . $group['title'] . ' </span></h3>';

                        echo '<div>';

                        foreach ( $this->field['fields'] as $field ) {
                            if($field['id'] == 'tab_post_type'){
                                echo '<div class="wpautbox-tab_post_type wpautbox-hidden">';
                            }
                            if($field['id'] == 'tab_type'){
                                echo '<div class="wpautbox-tab_type">';
                            }
                            if($field['id'] == 'tab_contact_form'){
                                echo '<div class="wpautbox-tab_contact_form wpautbox-hidden">';
                            }
                            
                            $this->output_field( $field, $x );
                            if($field['id'] == 'tab_post_type'){
                                echo '</div>';
                            }
                            if($field['id'] == 'tab_type'){
                                echo '</div>';
                            }
                            if($field['id'] == 'tab_contact_form'){
                                echo '</div>';
                            }
                        }
                        echo '<a href="javascript:void(0);" class="button deletion redux-groups-remove">' . __( 'Delete', 'redux-framework' ) . ' ' . $this->field['groupname'] . '</a>';
                        echo '</div>';
                        echo '</fieldset>';
                        echo '</table>';
                        echo '</div>';

                        $x ++;
                    }
                }

                if ( $x == 0 ) {
                    if(!isset($group)){
                        $group = array();
                    }
                    echo '<div class="redux-group-accordion-group">';
                    echo '<table style="margin-top: 0;" class="redux-group-accordion redux-group form-table no-border">';
                    echo '<fieldset class="redux-field" data-id="' . $this->field['id'] . '">';

                    echo '<input type="hidden" name="' . $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][redux_group_data][' . $x . '][title]" value="' . esc_attr( isset($this->value['redux_group_data'][0]['title']) ? $this->value['redux_group_data'][0]['title'] : ' ' ) . '" class="regular-text slide-title" />';

                    echo '<h3><span class="redux-group-header">' . ( isset($group['title']) ? $group['title'] : ' ' ) . ' </span><i class="group_field_edit_icon el-icon-pencil"></i></h3>';
                    echo '<div>';

                    foreach ( $this->field['fields'] as $field ) {
                        if($field['id'] == 'tab_post_type'){
                            echo '<div class="wpautbox-tab_post_type">';
                        }
                        if($field['id'] == 'tab_type'){
                            echo '<div class="wpautbox-tab_type">';
                        }
                        if($field['id'] == 'tab_contact_form'){
                            echo '<div class="wpautbox-tab_contact_form">';
                        }
                        $this->output_field( $field, $x );
                        if($field['id'] == 'tab_post_type'){
                            echo '</div>';
                        }
                        if($field['id'] == 'tab_type'){
                            echo '</div>';
                        }
                        if($field['id'] == 'tab_contact_form'){
                            echo '</div>';
                        }
                    }
                    echo '<a href="javascript:void(0);" class="button deletion redux-groups-remove">' . __( 'Delete', 'redux-framework' ) . ' ' . $this->field['groupname'] . '</a>';

                    echo '</div>';
                    echo '</fieldset>';
                    echo '</table>';
                    echo '</div>';
                }

                echo '</div>';
                echo '<a href="javascript:void(0);" class="button redux-groups-add button-primary" rel-id="' . $this->field['id'] . '-ul" rel-name="' . $this->parent->args['opt_name'] . $this->group_values . '[title][]">' . __( 'Add', 'redux-framework' ) . ' ' . $this->field['groupname'] . '</a><br/>';


            }

            /**
             * Enqueue Function.
             * If this field requires any scripts, or css define this function and register/enqueue the scripts/css
             *
             * @since       1.0.0
             * @access      public
             * @return      void
             */
            public function enqueue() {

                $extension = ReduxFramework_extension_group::getInstance();

                // Set up min files for dev_mode = false.
                $min = Redux_Functions::isMin();

                wp_enqueue_script(
                    'redux-field-group-js',
                    $this->extension_url . 'field_group' . $min . '.js',
                    array( 'jquery', 'jquery-ui-core', 'jquery-ui-accordion', 'wp-color-picker' ),
                    time(),
                    true
                );


                wp_enqueue_style(
                    'redux-field-group-css',
                    $this->extension_url . 'field_group.css',
                    time(),
                    true
                );
            }

            public function output_field( $field, $x ) {
                //we will enqueue all CSS/JS for sub fields if it wasn't enqueued
                $this->enqueue_dependencies( $field['type'] );

                //echo '<tr><td>';

                if ( isset( $field['class'] ) ) {
                    $field['class'] .= " group";
                } else {
                    $field['class'] = " group";
                }

                if ( ! empty( $field['title'] ) ) {
                    echo '<h4>' . $field['title'] . '</h4>';
                }

                if ( ! empty( $field['subtitle'] ) ) {
                    echo '<span class="description">' . $field['subtitle'] . '</span>';
                }

                $origFieldID = $field['id'];

                $field['id']          = $field['id'] . '-' . $x;
                $field["name"]        = $this->parent->args['opt_name'] . $this->group_values . '[' . $origFieldID . ']';
                $field['name_suffix'] = "[" . $x . "]";



                if ( isset( $field['default'] ) ) {
                    $default = $field['default'];
                } elseif ( isset( $field['options'] ) && ( $field['type'] != "ace_editor" ) ) {
                    // Sorter data filter
                    if ( $field['type'] == "sorter" && isset( $field['data'] ) && ! empty( $field['data'] ) && is_array( $field['data'] ) ) {
                        if ( ! isset( $field['args'] ) ) {
                            $field['args'] = array();
                        }
                        foreach ( $field['data'] as $key => $data ) {
                            if ( ! isset( $field['args'][ $key ] ) ) {
                                $field['args'][ $key ] = array();
                            }
                            $field['options'][ $key ] = $this->get_wordpress_data( $data, $field['args'][ $key ] );
                        }
                    }
                    $default = $field['options'];
                }

                $default = isset( $field['default'] ) ? $field['default'] : '';

                if ( ! empty( $this->group_values ) ) {
                    $value = !isset( $this->parent->options[ $this->field['id'] ][ $origFieldID ][ $x ] ) ? $default : $this->parent->options[ $this->field['id'] ][ $origFieldID ][ $x ];
                } else {
                    $value = !isset( $this->parent->options[ $origFieldID ][ $x ] ) ? $default : $this->parent->options[ $origFieldID ][ $x ];
                }


                ob_start();
                $this->parent->_field_input( $field, $value );

                $content = ob_get_contents();

                if ( ( $field['type'] === "text" ) && ( $field_is_title ) ) {
                    $content        = str_replace( '>', 'data-title="true" />', $content );
                    $field_is_title = false;
                }

                $_field = apply_filters( 'redux-support-group', $content, $field, 0 );
                ob_end_clean();
                echo $_field;

            }

            /**
             * Functions to pass data from the PHP to the JS at render time.
             *
             * @return array Params to be saved as a javascript object accessable to the UI.
             * @since  Redux_Framework 3.1.5
             */
            function localize( $field, $value = "" ) {

                if ( isset( $field['subfields'] ) && empty( $field['fields'] ) ) {
                    $field['fields'] = $field['subfields'];
                    unset( $field['subfields'] );
                }

                if ( isset( $field['group_values'] ) && $field['group_values'] ) {
                    $this->group_values = '[' . $field['id'] . ']';
                }

                $var = "";
                if ( isset( $field['fields'] ) && ! empty( $field['fields'] ) ) {
                    ob_start();
                    foreach ( $field['fields'] as $f ) {
                        $this->output_field( $f, 99999 );
                    }
                    $var = ob_get_contents();

                    if ( ! isset( $field['groupname'] ) ) {
                        $field['groupname'] = "";
                    }

                    $var = array(
                        'html'  => $var . '<a href="javascript:void(0);" class="button deletion redux-groups-remove">Delete </a>',
                        'count' => count( $value ),
                        'name'  => $this->parent->args['opt_name'] . '[' . $field['id'] . '][99999]',
                        'title' => __( 'New', 'redux-framework' ) . ' ' . $field['groupname'],
                    );
                    ob_end_clean();
                }

                return $var;
            }

            private function enqueue_dependencies( $field_type ) {
                $field_class = 'ReduxFramework_' . $field_type;

                if ( ! class_exists( $field_class ) ) {
                    $class_file = apply_filters( 'redux-typeclass-load', ReduxFramework::$_dir . 'inc/fields/' . $field_type . '/field_' . $field_type . '.php', $field_class );

                    if ( $class_file ) {
                        /** @noinspection PhpIncludeInspection */
                        require_once( $class_file );
                    }
                }

                if ( class_exists( $field_class ) && method_exists( $field_class, 'enqueue' ) ) {
                    $enqueue = new $field_class( '', '', $this );
                    $enqueue->enqueue();
                }
            }
        }
    }