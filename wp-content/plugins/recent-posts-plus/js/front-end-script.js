(function(){

	/**
	 * rppw : recent post plus widget
	 */
	var rppw_container = jQuery('[id*=recent-posts-plus]');


	// do not process further if no rppw exist on page
	if( ! rppw_container.length ){
		return;
	}


	/**
	 *  hide ajax loader
	 *  show ul list of posts
	 */
	var show_list = function( obj ){
		// hide ajax loader image
		jQuery( '.ajax-loader', obj ).hide();

		// show modified posts html
		jQuery( 'ul', obj ).show();
	};

	// do not process further if localstorage is not supported by browser
	if( ! window.localStorage ){

		// hide each ajax loader gif
		// and show post list
		rppw_container.each( function( i, k ){

			show_list( jQuery( this ) );
		});

		return;
	}

	// rearrage posts in list
	rppw_container.each( function( i, k ){
		// unique key for local storage
		var id_name = jQuery( this ).attr( 'id' );

		// post ids
		var post_ids = JSON.parse( localStorage.getItem( id_name ) );

		// check item already exist in local storage, if not then break this loop
		if( ! post_ids ){

			show_list( jQuery( this ) );
			return;
		}

		var posts_html = '';

		for ( id in post_ids ){

			var post_link_ctnr = jQuery( '[data-id="'+ post_ids[id] +'"]', jQuery( this ) );

			// check if post id exist in list
			if( post_link_ctnr.length ){
				var post_container = post_link_ctnr.parents('li');
				posts_html += '<li>' + post_container.html() + '</li>' ;
				post_container.remove();
			}
		}

		jQuery( 'ul', this ).append( posts_html );
		show_list( jQuery( this ) );
	});

	// add event to each anchor link with class 'post-link'
	rppw_container.each( function( i, k ){

		// unique key for local storage
		var id_name = jQuery( this ).attr( 'id' );

		jQuery( this ).on( 'click', 'a.post-link', function( e ){
			e.preventDefault();

			

			var post_ids = JSON.parse( localStorage.getItem( id_name ) );

				
			// check item already exist in local storage, if not then intialize empty array
			if( ! post_ids ){
				post_ids = [];
			}

			var post_id = jQuery(this).data( 'id' );

			// check if value already exist in array
			if( jQuery.inArray( post_id, post_ids ) > -1 ){
				window.location.assign( jQuery( this ).attr( 'href' ) );
				return;
			}

			// push current post id to array
			post_ids.push( post_id );

			// check count of post id array
			// if count > 200 then remove first id  

			if( post_ids.length > 200 ){
				for ( var i = 0; i < 1; i++ ) {
					// remove post id
					post_ids.shift();
				}
			}

			// store data in local storage
			localStorage.setItem( id_name, JSON.stringify( post_ids ) );

			window.location.assign( jQuery( this ).attr( 'href' ) );
			return;
		});
	});

})();