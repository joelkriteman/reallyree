Instructions:

[Note: replace current plugin with new plugin ]
(apply this only for post link not to other links )
1. add class to post link
2. add data attribute to post link and provide post id to it

demo template:

<li>
   <a class="post-link" data-id="{ID}" title="{TITLE_RAW}" href="{PERMALINK}">{THUMBNAIL}</a>
   <h3 class="rpwe-title">
      <a class="post-link" data-id="{ID}" title="{TITLE_RAW}" href="{PERMALINK}">{TITLE}</a>
   </h3>
</li>