<?php
/*
	Plugin Name: WP Most Popular - Offset Extension
	Plugin URI: http://mattgeri.com
	Description: A widget extension for the WP Most Popular plugin which creates a reusable widget with an offset parameter
	Version: 1.0
	Author: Matt Geri
	Author URI: http://mattgeri.com
*/

// We only want this extension to be available if WordPress exists
if ( class_exists( 'WMP_system' ) && ! class_exists( 'WMPOE_Widget' ) ) {
	add_action( 'widgets_init', 'wpoe_register_widget' );

	function wpoe_register_widget() {
		// Initilize our widget
		register_widget( 'wmpoe_widget' );
	}

	class WMPOE_Widget extends WP_Widget {
		// Register our widget
		public function __construct() {
			parent::WP_Widget( 'wmpoe_widget', 'WP Most Popular - Offset', array( 'description' => 'A custom WP Most Popular extension which lets you display popular posts with an offset' ) );
		}
		
		// Create the form for the widget with the option to show numbers as well as display specific results
		public function form( $instance ) {
			$defaults = $this->default_options( $instance );
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br />
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $defaults['title']; ?>">
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'from' ); ?>">Show posts:</label><br />
				<input id="<?php echo $this->get_field_id( 'from' ); ?>" name="<?php echo $this->get_field_name( 'from' ); ?>" type="text" value="<?php echo $defaults['from']; ?>" size="3">
				to
				<input id="<?php echo $this->get_field_id( 'to' ); ?>" name="<?php echo $this->get_field_name( 'to' ); ?>" type="text" value="<?php echo $defaults['to']; ?>" size="3">
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'show_ranking' ); ?>">Show ranking:</label><br />
				<select id="<?php echo $this->get_field_id( 'show_ranking' ); ?>" name="<?php echo $this->get_field_name( 'show_ranking' ); ?>">
					<option value="no">No</option>
					<?php $defaults['show_ranking'] == 'yes' ? $sel = " selected" : $sel = ""; ?>
					<option value="yes"<?php echo $sel; ?>>Yes</option>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'post_type' ); ?>">Choose post type:</label><br />
				<select id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>">
					<option value="all">All post types</option>
					<?php
					$post_types = get_post_types( array( 'public' => true ), 'names' );
					foreach ($post_types as $post_type ) {
						// Exclude attachments
						if ( $post_type == 'attachment' ) continue;
						$defaults['post_type'] == $post_type ? $sel = " selected" : $sel = "";
						echo '<option value="' . $post_type . '"' . $sel . '>' . $post_type . '</option>';
					}
					?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'timeline' ); ?>">Timeline:</label><br />
				<select id="<?php echo $this->get_field_id( 'timeline' ); ?>" name="<?php echo $this->get_field_name( 'timeline' ); ?>">
					<option value="all_time"<?php if ( $defaults['timeline'] == 'all_time' ) echo "selected"; ?>>All time</option>
					<option value="monthly"<?php if ( $defaults['timeline'] == 'monthly' ) echo "selected"; ?>>Past month</option>
					<option value="weekly"<?php if ( $defaults['timeline'] == 'weekly' ) echo "selected"; ?>>Past week</option>
					<option value="daily"<?php if ( $defaults['timeline'] == 'daily' ) echo "selected"; ?>>Today</option>
				</select>
			</p>
			<?php
		}
		
		// Returns default options for our plugin if no options exist already
		private function default_options( $instance ) {
			if ( isset( $instance[ 'title' ] ) )
				$options['title'] = esc_attr( $instance[ 'title' ] );
			else
				$options['title'] = 'Popular posts';
				
			if ( isset( $instance[ 'from' ] ) )
				$options['from'] = (int) $instance[ 'from' ];
			else
				$options['from'] = 1;
				
			if ( isset( $instance[ 'to' ] ) )
				$options['to'] = (int) $instance[ 'to' ];
			else
				$options['to'] = 5;
			
			if ( isset( $instance[ 'show_ranking' ] ) )
				$options['show_ranking'] = esc_attr( $instance[ 'show_ranking' ] );
			else
				$options['show_ranking'] = 'no';
			
			if ( isset( $instance[ 'post_type' ] ) )
				$options['post_type'] = esc_attr( $instance[ 'post_type' ] );
			else
				$options['post_type'] = 'all';

			if ( isset( $instance[ 'timeline' ] ) )
				$options['timeline'] = esc_attr( $instance[ 'timeline' ] );
			else
				$options['timeline'] = 'all_time';
			
			return $options;
		}
		
		// Triggered when the widget is saved
		public function update( $new, $old ) {
			$instance = wp_parse_args( $new, $old );
			return $instance;
		}
		
		// The actual display of the widget
		public function widget( $args, $instance ) {
			// Find default args
			extract( $args );
			
			// Get our posts
			$defaults			= $this->default_options( $instance );
			$options['range']	= $defaults['timeline'];

			$to		= (int) $defaults[ 'to' ];
			$from	= (int) $defaults[ 'from' ];

			// Work out the offset and limit
			if ( $to > 0 && $from > 0 && $to >= $from ) {
				$offset	= $from - 1;
				$limit	= $to - $from + 1;
			} else {
				$offset	= 0;
				$limit	= 0;
			}

			$options['offset']	= $offset;
			$options['limit']	= $limit;

			if ( $defaults['post_type'] != 'all' ) {
				$options['post_type'] = $defaults['post_type'];
			}

			if ( $offset >= 0 && $limit > 0 ) {
				$posts = wmp_get_popular( $options );
				$rank = $from;
				
				// Display the widget
				echo $before_widget;
				if ( $defaults['title'] ) echo $before_title . $defaults['title'] . $after_title;
				echo '<ul>';
				global $post;
				foreach ( $posts as $post ):
					setup_postdata( $post );
					$defaults['show_ranking'] == "yes" ? $ranking = $rank . "" : $ranking = "";
					?>
					<li><span class="rank"><?php echo $ranking; ?></span>
    					<a href="<?php the_permalink(); ?>">
    						<h3 class="onpost"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></h3>
    							<?php the_post_thumbnail('post-thumbnail'); ?></a></li>
					
					<?php
					$rank++;
				endforeach;
				echo '</ul>';
				echo $after_widget;
				
				// Reset post data
				wp_reset_postdata();
			}
		}
	}
}