<?php
/*
  Plugin Name: Share Rating
  Plugin URI: https://www.elementous.com
  Description: This is an extension for Rating Manager plugin. It allows you to share a post rating on Twitter. 
  Author: Elementous
  Author URI: https://www.elementous.com
  Version: 1.0
*/

define( 'ELM_SR_VERSION', '1.0' );
define( 'ELM_SR_PLUGIN_PATH', dirname( __FILE__ ) );
define( 'ELM_SR_PLUGIN_FOLDER', basename( ELM_SR_PLUGIN_PATH ) );
define( 'ELM_SR_PLUGIN_URL', plugins_url() . '/' . ELM_SR_PLUGIN_FOLDER );

class Elm_Share_Rating {

	function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'wp_enqueue_scripts', array( $this,  'enqueue_scripts'  ) );

		add_action( 'wp_head', array( $this, 'wp_head' ) );
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );

		add_action( 'elm_ur_html_template', array( $this, 'elm_ur_after_html_template' ) );
		
		add_filter( 'elm_ur_ultimate_ratings_js_url', array($this, 'ultimate_ratings_js_url' ));
	}

	function ultimate_ratings_js_url( $url ) {
		return ELM_SR_PLUGIN_URL . '/assets/js/rating-manager.js';
	}

	function init() {
		
	}

	function wp_footer() {
		global $post;

		echo "<script type=\"text/javascript\">
		var sharer_post_title = \"". $post->post_title ."\";
		var sharer_post_url = '". get_permalink($post->ID) ."';

		</script>";

	}	

	function elm_ur_after_html_template($content) {
		if(!is_admin()){
			$content .= $this->modal( get_the_ID() );

			//echo $template;
		}

		return $content;
	}

	function modal($id) {
		return '
			<div class="modal fade js-share-modal" id="myModal-post-'. $id .'" role="dialog">
			    <div class="modal-dialog">
			    
			      <!-- Modal content-->
			      <div class="modal-content">
			      	<div class="modal-header" style="border-bottom: none; border-left: 2px solid #e62e87; border-right: 2px solid #e62e87; border-top: 2px solid #e62e87;">
			          <button type="button" class="btn btn-default" data-dismiss="modal" style="float: right;">X</button>
			      	</div>

			        <div class="modal-body" style="text-align: center; border-left: 2px solid #e62e87; border-right: 2px solid #e62e87; border-bottom: 2px solid #e62e87;">
			        <img class="sharelogo" src="http://www.reallyree.com/wp-content/themes/reallyree/images/ReallyRee-logo.png" alt="" />
			        	<div class="clear"></div><br />
			          <span class="i-just-rated" style="font-size: 14px; color: #e62e87;">I JUST RATED</span><br />
			          <span class="modal-post-title" style="font-size: 21px; font-weight: bold; color: #e62e87;"></span><br />


			          <div class="modal-stars" style="width: 150px; margin: 0 auto;"></div>

			          <br /><br />
			          
						<a class="twitter-timeline2"
						  href="https://twitter.com/intent/tweet?text=I just rated '. $post->post_title .' on @ReallyRee '. get_permalink($id) .'"
						  data-size="large" target="_blank">
						Share on Twitter</a>

			          
			        </div>
			      </div>
			      
			    </div>
		  </div>';
	}

	function wp_head() {
		// Bootstrap for modals
		echo '<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>';
	}	

	function enqueue_scripts() {
		wp_enqueue_style( 'sharerbootstrap', ELM_SR_PLUGIN_URL . '/assets/css/bootstrap.css' );
		wp_enqueue_style( 'sharer', ELM_SR_PLUGIN_URL . '/assets/css/share-rating.css' );
		wp_enqueue_script( 'elm-share-rating', ELM_SR_PLUGIN_URL . '/assets/js/share-rating.js', array( 'jquery' ) );
	}
}

$elm_share_rating = new Elm_Share_Rating;