msgid ""
msgstr ""
"Project-Id-Version: Dynamic Forms\n"
"Report-Msgid-Bugs-To: http://wordpress.org/tag/category-custom-post-order\n"
"POT-Creation-Date: 2015-12-04 08:47+0100\n"
"PO-Revision-Date: 2015-12-04 08:48+0100\n"
"Last-Translator: Piotr Potrebka <piotr.potrebka@men.gov.pl>\n"
"Language-Team: \n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _;__;_e;esc_html_e\n"
"X-Poedit-Basepath: .\n"
"X-Generator: Poedit 1.6.5\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../category-post-sorter.php:128 ../category-post-sorter.php:523
msgid "Default"
msgstr "Domyślnie"

#: ../category-post-sorter.php:129
msgid "Custom"
msgstr "Własne"

#: ../category-post-sorter.php:130 ../category-post-sorter.php:147
msgid "Date"
msgstr "Data"

#: ../category-post-sorter.php:131
msgid "Modified"
msgstr "Modyfikacja"

#: ../category-post-sorter.php:132
msgid "Title"
msgstr "Tytuł"

#: ../category-post-sorter.php:133
msgid "ID"
msgstr "ID"

#: ../category-post-sorter.php:134
msgid "Author"
msgstr "Autor"

#: ../category-post-sorter.php:135
msgid "Custom Field"
msgstr "Własne pole"

#: ../category-post-sorter.php:141
msgid "Descending"
msgstr "Malejąco"

#: ../category-post-sorter.php:142
msgid "Ascending"
msgstr "Rosnąco"

#: ../category-post-sorter.php:146
msgid "Char"
msgstr "Char"

#: ../category-post-sorter.php:148
msgid "Datetime"
msgstr "Datetime"

#: ../category-post-sorter.php:149
msgid "Integer"
msgstr "Integer"

#: ../category-post-sorter.php:248 ../category-post-sorter.php:305
#: ../category-post-sorter.php:518
msgid "Order posts"
msgstr "Sortuj wpisy"

#: ../category-post-sorter.php:309
msgid "Category:"
msgstr "Kategoria:"

#: ../category-post-sorter.php:322
msgid "No posts"
msgstr "Brak wpisów"

#: ../category-post-sorter.php:326
msgid "Reorder"
msgstr "Posortuj"

#: ../category-post-sorter.php:328
msgid "Remove order"
msgstr "Usuń sortowanie"

#: ../category-post-sorter.php:330
msgid "Reverse"
msgstr "Odwróć"

#: ../category-post-sorter.php:369 ../category-post-sorter.php:421
msgid "Order by"
msgstr "Sortuj wg"

#: ../category-post-sorter.php:387 ../category-post-sorter.php:440
msgid "&mdash; Select &mdash;"
msgstr "&mdash; Wybierz &mdash;"

#: ../category-post-sorter.php:396 ../category-post-sorter.php:449
msgid "Type"
msgstr "Typ"

#: ../category-post-sorter.php:403 ../category-post-sorter.php:457
msgid "Select order field"
msgstr "Wybierz sposób sortowania"

#: ../category-post-sorter.php:497
msgid "Order"
msgstr "Sortowanie"

#~ msgid "None"
#~ msgstr "Brak"

#~ msgid "Descending "
#~ msgstr "Malejąco [Z-A]"

#~ msgid "Ascending "
#~ msgstr "Rosnąco [A-Z]"
