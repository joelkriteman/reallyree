var moi_stop_popup = false;
var moi_timeinterval = 20000;

setTimeout(function() {

    //stop seeing popup
    if( moi_stop_popup  ){
        return;
    }

    document.getElementById('moi_overlay').style.display = 'block';
    document.getElementById('moi_signup_popup').style.display = 'block';

}, moi_timeinterval );


jQuery(document).ready(function(){
   jQuery('a.moi_close').on('click',function(e){
       e.preventDefault();

       var expire_date = new Date();
       expire_date.setDate( expire_date.getDate() + 7 );

       var email = jQuery('input[type="text"]', '#moi_signup_popup').val();
       email = email.split('@');
       email = email[0];

       document.cookie = "showpopup=1; expires="+expire_date+"; path=/";

       document.getElementById('moi_overlay').style.display = 'none';
       document.getElementById('moi_signup_popup').style.display = 'none';
       return false;
   });

   jQuery('input[type="submit"]', '#moi_signup_popup' ).on('click',function(e){

       if( jQuery('input[type="text"]', '#moi_signup_popup').val() === '' ){
           return;
       }

       var expire_date = new Date();
       expire_date.setDate( expire_date.getYear() + (100*365) );

       var email = jQuery('input[type="text"]', '#moi_signup_popup').val();
       email = email.split('@');
       email = email[0];

       document.cookie = "user_subscribed="+email+"; expires="+expire_date+"; path=/";

       document.getElementById('moi_overlay').style.display = 'none';
       document.getElementById('moi_signup_popup').style.display = 'none';
   });

    jQuery( 'form', '#moi_signup_popup').on('submit',function(){

        if( jQuery('input[type="text"]', '#moi_signup_popup').val() === '' ){
            alert('Please enter your email address.');
            return false;
        }
        window.open('http://feedburner.google.com/fb/a/mailverify?uri=ReallyreeFashion/beautyBlog', 'popupwindow', 'scrollbars=yes,width=550,height=520');
        return true;
    });
});