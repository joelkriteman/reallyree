<?php

/*
Plugin Name: Signup popup
Plugin URI: http://monksinc.co
Description:
Version: 0.1
Author: The Monksinc team
Author URI: http://monksinc.co
License: GPL2
*/

function moi_signup_script(){

	// if visiter rejecte dto subscribe or already subscribe then do not show popup
	if( isset( $_COOKIE['showpopup'] ) || isset( $_COOKIE['user_subscribed'] ) ){
		return;
	}

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'moi-signup-js', plugins_url( 'js/script.js', __FILE__ ), array('jquery') );
	wp_enqueue_style( 'moi-signup-css', plugins_url( 'css/style.css', __FILE__ ) );
}
add_action( 'wp_enqueue_scripts', 'moi_signup_script' );


//signup popup html
function moi_signup_fomr_html(){

	// if visiter rejecte to subscribe or already subscribe then do not show popup
	if( isset( $_COOKIE['showpopup'] ) || isset( $_COOKIE['user_subscribed'] ) ){
		return;
	}

	?>
	<div id="moi_overlay" style="display: none;"></div>
	<div id="moi_signup_popup" style="display: none;">
		<div class="moi_outer_content">
			<div class="moi_inner_content">
				<a href="#" class="moi_close">&#215;</a>
				<img src="<?php echo plugins_url( 'images/SignUp-Portrait.jpg', __FILE__ ); ?>" alt="signup logo image"/>
				<div id="formright"><h1>Sign Up</h1><h5>for all the latest</h5>
				<p>Beauty News, Product<br />Reviews, Swatches<br />Great offers & Giveaways<br />straight to your inbox</p>
					<form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow">
					<ul>
						<li><input type="text" name="email" placeholder="Email address"/></li>
						<li><input type="hidden" value="ReallyreeFashion/beautyBlog" name="uri"/></li>
						<li><input type="hidden" name="loc" value="en_US"/></li>
						<li><input type="submit" value="GO" /></li>
					</ul>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
}
add_action( 'wp_footer', 'moi_signup_fomr_html' );