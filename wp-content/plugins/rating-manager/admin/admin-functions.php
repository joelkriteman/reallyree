<?php
/*
 * This file contains admin UI functions
 */
 
 /*
 * Get icon list
 */
function elm_ur_icon_list() {
	$array = array(
		'award-1' => __('Award 1 ', 'elm'),
		'award-2' => __('Award 2 ', 'elm'),
		'award-3' => __('Award 3 ', 'elm'),
		'award-4' => __('Award 4 ', 'elm'),
		'award-5' => __('Award 5 ', 'elm'),
		'badge-1' => __('Badge 1 ', 'elm'),
		'badge-2' => __('Badge 2 ', 'elm'),
		'badge-3' => __('Badge 3 ', 'elm'),
		'badge-4' => __('Badge 4 ', 'elm'),
		'badge-5' => __('Badge 5 ', 'elm'),
		'beer' => __('Beer jar ', 'elm'),
		'bone' => __('Bone ', 'elm'),
		'bulb' => __('Bulb ', 'elm'),
		'camera' => __('Camera ', 'elm'),
		'chemistry' => __('Chemistry ', 'elm'),
		'clipboard' => __('Clipboard ', 'elm'),
		'coffee' => __('Coffee cup ', 'elm'),
		'crown' => __('Crown ', 'elm'),
		'diamond-1' => __('Diamond 1 ', 'elm'),
		'diamond-2' => __('Diamond 2 ', 'elm'),
		'dog' => __('Dog ', 'elm'),
		'dress' => __('Dress ', 'elm'),
		'drop' => __('Drop ', 'elm'),
		'flash' => __('Flash ', 'elm'),
		'flower-1' => __('Flower 1 ', 'elm'),
		'flower-2' => __('Flower 2 ', 'elm'),
		'flower-3' => __('Flower 3 ', 'elm'),
		'heart-1' => __('Heart 1 ', 'elm'),
		'heart-2' => __('Heart 2 ', 'elm'),
		'heart-3' => __('Heart 3 ', 'elm'),
		'heart-4' => __('Heart 4 ', 'elm'),
		'leaf' => __('Leaf ', 'elm'),
		'like-1' => __('Like 1 ', 'elm'),
		'like-2' => __('Like 2 ', 'elm'),
		'lips' => __('Woman lips ', 'elm'),
		'note' => __('Note ', 'elm'),
		'pacman' => __('Pacman ', 'elm'),
		'pepper' => __('Pepper ', 'elm'),
		'plus-1' => __('Plus 1 ', 'elm'),
		'plus-2' => __('Plus 2 ', 'elm'),
		'plus-3' => __('Plus 3 ', 'elm'),
		'radioactive-sign' => __('Radioactive ', 'elm'),
		'ring-1' => __('Ring 1 ', 'elm'),
		'ring-2' => __('Ring 2 ', 'elm'),
		'skull-bones' => __('Skull ', 'elm'),
		'smile-1' => __('Smile 1 ', 'elm'),
		'smile-2' => __('Smile 2 ', 'elm'),
		'smile-3' => __('Smile 3 ', 'elm'),
		'soccer-ball' => __('Ball ', 'elm'),
		'star-1' => __('Star 1 ', 'elm'),
		'star-2' => __('Star 2 ', 'elm'),
		'star-3' => __('Star 3 ', 'elm'),
		'star-4' => __('Star 4 ', 'elm'),
		'star-5' => __('Star 5 ', 'elm'),
		'star-6' => __('Star 6 ', 'elm'),
		'star-7' => __('Star 7 ', 'elm'),
		'star-8' => __('Star 8 ', 'elm'),
		'sun' => __('Sun ', 'elm'),
		'target' => __('Target ', 'elm'),
		'tick-1' => __('Tick 1 ', 'elm'),
		'tick-2' => __('Tick 2 ', 'elm'),
		'tick-3' => __('Tick 3 ', 'elm'),
		'tick-4' => __('Tick 4 ', 'elm'),
		'tick-5' => __('Tick 5 ', 'elm'),
		'tick-6' => __('Tick 6 ', 'elm'),
		'tick-7' => __('Tick 7 ', 'elm'),
		'tick-8' => __('Tick 8 ', 'elm'),
		'tick-9' => __('Tick 9 ', 'elm'),
		'trophy-1' => __('Trophy 1 ', 'elm'),
		'trophy-2' => __('Trophy 2 ', 'elm'),
	);

	return $array;
}

/*
 * Get font family list
 */
function elm_ur_font_family_list() {
	$array = array(
		'Georgia' => 'Georgia',
		'Palatino Linotype' => 'Palatino Linotype',
		'Times New Roman' => 'Times New Roman',
		'Arial' => 'Arial',
		'Arial Black' => 'Arial Black',
		'Comic Sans MS' => 'Comic Sans MS',
		'Impact' => 'Impact',
		'Lucida Sans Unicode' => 'Lucida Sans Unicode',
		'Tahoma' => 'Tahoma',
		'Trebuchet MS' => 'Trebuchet MS',
		'Verdana' => 'Verdana',
		'Courier New' => 'Courier New',
		'Lucida Console' => 'Lucida Console'
	);

	return $array;
}

/*
 * Get font style list
 */
function elm_ur_font_style_list() {
	$array = array(
		'normal' => 'Normal',
		'italic' => 'Italic'
	);

	return $array;
}

/*
 * Get font size list
 */
function elm_ur_font_size_list() {
	
	for( $i = 1; $i <= 35; $i++ ) {
		$px = $i . 'px';
		
		$array[$px] = $px;
	}

	return $array;
}

/*
 * Get image size list
 */
function elm_ur_image_size() {
	for( $i = 10; $i <= 100; $i++ ) {
		$px = $i . 'px';
		
		$array[$px] = $px;
	}

	return $array;
}

/*
 * Get custom SVG file source
 */
function elm_get_custom_svg() {
	$custom_svg = ELM_UR_PLUGIN_SVG_PATH . '/custom.svg';
	
	if ( file_exists( $custom_svg ) ) {
		$string = file_get_contents($custom_svg);
		
		return $string;
	}
	
	return '';
}
