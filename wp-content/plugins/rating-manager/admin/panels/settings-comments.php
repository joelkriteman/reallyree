<?php 
/*
 * Display admin comments page
*/

// don't load directly
if ( !defined('ABSPATH') )
	exit;
 
global $elm_ur_ratings;
$settings = $elm_ur_ratings->get_settings->get_settings();
?>

<div class="wrap rating-manager">
	<?php $elm_ur_ratings->get_settings_gui->messages_html(); ?>
		
	<?php $elm_ur_ratings->get_settings_gui->tabs_html(); ?>

    <h3><?php _e('Comment rating settings', 'elm'); ?></h3>

    <form action="" method="post">
        <table class="form-table">
			<tr>
                <th scope="row">
                    <label><?php _e('Enable comment rating on', 'elm'); ?></label>
                </th>
                <td>
				
				<fieldset><legend class="screen-reader-text"><span><?php _e('Enable comment rating on', 'elm'); ?></span></legend>
				<?php
				$options = $elm_ur_ratings->get_custom_post_types();
				
				foreach ( $options as $key => $value ) :
					echo '<label for="allow_comment_rating_on['. $key .']"><input type="checkbox" name="allow_comment_rating_on['. $key .']" id="allow_comment_rating_on['. $key .']" value="'. $key .'" '. checked( @$settings['comments']['allow_comment_rating_on'][$key], 1, false ) .' />
						'. $value .'</label><br />';
				endforeach;
				?>
				</fieldset>
			
				<p class="description"><?php _e('Enable comment rating on preferred post types.', 'elm'); ?></p>
                </td>
            </tr>
			<tr>
                <th scope="row">
                    <label><?php _e('Logging', 'elm'); ?></label>
                </th>
                <td>
				<select name="logging" id="logging" class="width-200">
				<?php
				$options = array( 'cookie' => __('Cookie', 'elm'), 'ip' => __('IP', 'elm'), 'cookie_and_ip' => __('Cookie and IP', 'elm'), 'username' => __('Username', 'elm') );
				
				foreach( $options as $key => $value ) :
					$selected = ( $settings['comments']['logging'] == $key ) ? 'selected' : '';
					echo '<option value="'. $key .'" '. $selected .'>'. $value .'</option>' . "\r\n";
				endforeach;
				
				?>
				</select>
				
				<p class="description"><?php _e('Select preferred logging strategy.', 'elm'); ?></p>
                </td>
            </tr>
			<tr>
                <th scope="row">
                    <label><?php _e('Rating mode', 'elm'); ?></label>
                </th>
                <td>
				<ul class="ratings-stars">
					<li><input type="radio" name="comment_rating_image" value="image1" <?php checked( $settings['comments']['rating_image'], 'image1' ); ?>><img src="<?php echo ELM_UR_PLUGIN_URL . '/assets/images/comments_image1.png'; ?>" class="rating-image" /></li>
					<li><input type="radio" name="comment_rating_image" value="image2" <?php checked( $settings['comments']['rating_image'], 'image2' ); ?>><img src="<?php echo ELM_UR_PLUGIN_URL . '/assets/images/comments_image2.png'; ?>" class="rating-image" />
					</li>
				</ul>
				
				<p class="description"><?php _e('Select preferred comment rating mode.', 'elm'); ?></p>
                </td>
            </tr>
			<tr>
                <th scope="row">
                    <label><?php _e('Icon size', 'elm'); ?></label>
                </th>
                <td>
				<select name="comments_image_size" id="comments_image_size">
					<?php
					$image_size_list = elm_ur_image_size();
					foreach($image_size_list as $k => $value ) {
						$selected = selected( $settings['comments']['image_size'], $k, false );
					
						echo '<option value="' . $k . '" '. $selected .'>'. $value .'</option>';
					}
					?>
				</select>
				
				<p class="description"><?php _e('Set size for rating form icons.', 'elm'); ?></p>
                </td>
            </tr>
			<tr>
                <th scope="row">
                    <label><?php _e('Icon colors', 'elm'); ?></label>
                </th>
                <td>
				
				<ul>
				<li>
				<div class="elm-ur-color" id="like-fill-color-picker">
					<label for="comment_like_fill_color_picker"><?php _e('Icon color (like)', 'elm'); ?></label><br />
					<div id="comment_like_fill_color_picker" class="colorSelector">
						<div></div>
					</div>
						<input class="elm-color small-text elm-typography elm-typography-color" name="comment_like_fill" id="comment_like_fill_color" type="text" value="<?php echo $settings['comments']['like_fill']; ?>" />
				</div>
				</li>
				
				<li>
				<div class="elm-ur-color" id="dislike-fill-color-picker">
					<label for="comment_dislike_fill_color_picker"><?php _e('Icon color (dislike)', 'elm'); ?></label><br />
					<div id="comment_dislike_fill_color_picker" class="colorSelector">
						<div></div>
					</div>
						<input class="elm-color small-text elm-typography elm-typography-color" name="comment_dislike_fill" id="comment_dislike_fill_color" type="text" value="<?php echo $settings['comments']['dislike_fill']; ?>" />
				</div>
				</li>
				</ul>
				
				<p class="description"><?php _e('Select colors for rating icons.', 'elm'); ?></p>
                </td>
            </tr>
			<tr>
                <th scope="row">
                    <label><?php _e('HTML template', 'elm'); ?></label>
                </th>
                <td>
				<textarea rows="10" cols="50" name="comment_template" id="comment-template" class="large-text code"><?php echo trim( $settings['comments']['template'] ); ?></textarea>
				<input type="button" name="reset_comments_html_template" id="reset-comments-html-template" class="button button-secondary" value="<?php _e('Default HTML template','elm'); ?>" />
				
				<p class="description"><?php _e('HTML template of comment rating form.', 'elm'); ?></p>
                </td>
            </tr>
			<tr>
                <th scope="row">
                    <label><?php _e('Top comments', 'elm'); ?></label>
                </th>
                <td>
				<fieldset><legend class="screen-reader-text"><span><?php _e('Top comments', 'elm'); ?></span></legend>
				
				<label for="top_comment[show_first_most_rated]">
				<input type="checkbox" name="top_comment[show_first_most_rated]" id="top_comment[show_first_most_rated]" value="1" <?php checked( $settings['comments']['top_comment']['show_first_most_rated'], '1' ); ?> />
				<?php _e('Show the most rated comment on top', 'elm'); ?></label><br />
				</fieldset>
				
				<p class="description"><?php _e('Highlight most rated comments.', 'elm'); ?></p>
                </td>
            </tr>
        </table>

		<?php wp_nonce_field( 'elm_ur_settings_comments_page_action', 'elm_ur_settings_comments_page_nonce' ); ?>
		
        <p class="submit">
            <input type="submit" name="elm_save_ur_settings_comments" id="submit" class="button button-primary" value="<?php _e('Save settings', 'elm'); ?>" />
        </p>
    </form>

</div>
