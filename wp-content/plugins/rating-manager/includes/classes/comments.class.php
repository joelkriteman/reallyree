<?php

if ( ! class_exists( 'Elm_UR_Comments' ) ) :

class Elm_UR_Comments {
    
    function __construct() {
        $this->get_settings = new Elm_UR_Settings;
		
		add_action( 'wp_ajax_elm_process_comment_rating', array( $this, 'process_comment_rating_callback' ) );
		add_action( 'wp_ajax_nopriv_elm_process_comment_rating', array( $this, 'process_comment_rating_callback' ) );
		
		if ( $this->get_settings->get_setting( 'comments', 'top_comment', 'show_first_most_rated' ) ) {
			add_filter( 'comments_array', array( $this, 'ro_change_order' ) );
		}
		
		add_action( 'comment_text', array( $this, 'display_comment_rating' ) );
    }

    /*
     * AJAX Comment rating call
     */
    function process_comment_rating_callback() {
		check_ajax_referer( 'elm_ur_process_comment_action', 'nonce' );
		
        $comment_id = (int) @$_POST['comment_id'];
		$post_id = (int) @$_POST['post_id'];
        $type       = sanitize_text_field( @$_POST['type'] );
		
		do_action( 'elm_ur_add_comment_rating_ajax_callback', $comment_id, $post_id, $type );
        
       if ( !$this->get_comment_log_rating( $this->get_comment_log_method(), $comment_id, $type ) ) {
            $this->add_comment_rating( $comment_id, $post_id, $type );
            
            $set = $this->set_comment_log_rating( $this->get_comment_log_method(), $comment_id, $type );
        }
        
        $response = array(
             'total_rating' => $this->calculate_comment_rating( $post_id, $comment_id ),
			 'like_rating' => $this->calculate_comment_rating( $post_id, $comment_id, 'like' ),
			 'dislike_rating' => $this->calculate_comment_rating( $post_id, $comment_id, 'dislike' )
        );
        
        echo json_encode( $response );
        
        die();
    }
    
    /*
     * Get comment logging method
     */
    function get_comment_log_method() {
        $method = $this->get_settings->get_setting( 'comments', 'logging' );
        
        return $method;
    }
    
    /**
     * Check if comment rating has been logged
	 *
     * @param string $method
     * @param int $comment_id
     */
    function get_comment_log_rating( $method, $comment_id, $type ) {
        global $elm_ur_ratings, $wpdb, $current_user;
		
        if ( $method == 'cookie' ) {
            if ( isset( $_COOKIE['elm_ur_comment_' . $comment_id] ) && $_COOKIE['elm_ur_comment_' . $comment_id] == $elm_ur_ratings->stats->get_cookie_value_prefix() . $comment_id ) {
                return true;
            } else {
                return false;
            }
        } else if ( $method == 'ip' ) {
            $ip = $_SERVER['REMOTE_ADDR'];
            
            $query = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}elm_ratings_log WHERE type = %s AND type_value = %s AND value = %d AND rating_type = %s", 'ip', $ip, $comment_id, 'comment' ) );
            
            if ( $query ) {
                return true;
            } else {
                return false;
            }
        } else if ( $method == 'cookie_and_ip' ) {
            $ip = $_SERVER['REMOTE_ADDR'];
            
            $query = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}elm_ratings_log WHERE type = %s AND type_value = %s AND value = %d AND rating_type = %s", 'ip', $ip, $comment_id, 'comment' ) );
            
            if ( isset( $_COOKIE['elm_ur_comment_' . $comment_id] ) && $_COOKIE['elm_ur_comment_' . $comment_id] == $elm_ur_ratings->stats->get_cookie_value_prefix() . $comment_id && $query ) {
                return true;
            } else {
                return false;
            }
            
        } else if ( $method == 'username' ) {
            $query = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}elm_ratings_log WHERE type = %s AND type_value = %s AND value = %d AND rating_type = %s", 'username', $current_user->user_login, $comment_id, 'comment' ) );
            
            if ( $query ) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    /**
     * Log comment rating by method and comment ID
	 *
     * @param string $method
     * @param int $comment_id
     */
    function set_comment_log_rating( $method, $comment_id, $type ) {
        global $elm_ur_ratings, $wpdb, $current_user;
        
        if ( $method == 'cookie' ) {
            setcookie( 'elm_ur_comment_' . $comment_id, $elm_ur_ratings->stats->get_cookie_value_prefix() . $comment_id, time() + ( 10 * 365 * 24 * 60 * 60 ), COOKIEPATH, COOKIE_DOMAIN );
        } else if ( $method == 'ip' ) {
            $ip = $_SERVER['REMOTE_ADDR'];
            
            $wpdb->insert( $wpdb->prefix . 'elm_ratings_log', array(
                 'type' => 'ip',
                'type_value' => $ip,
                'value' => $comment_id,
                'rating_type' => 'comment' 
            ), array(
                 '%s',
                '%s',
                '%d',
                '%s' 
            ) );
        } else if ( $method == 'cookie_and_ip' ) {
            setcookie( 'elm_ur_comment_' . $comment_id, $elm_ur_ratings->stats->get_cookie_value_prefix() . $comment_id, time() + ( 10 * 365 * 24 * 60 * 60 ), COOKIEPATH, COOKIE_DOMAIN );
            
            $ip = $_SERVER['REMOTE_ADDR'];

            $wpdb->insert( $wpdb->prefix . 'elm_ratings_log', array(
                'type' => 'ip',
                'type_value' => $ip,
                'value' => $comment_id,
                'rating_type' => 'comment'
            ), array(
                '%s',
                '%s',
                '%d',
                '%s'
            ) );
        } else if ( $method == 'username' ) {
            if ( is_user_logged_in() ) {
                $wpdb->insert( $wpdb->prefix . 'elm_ratings_log', array(
                     'type' => 'username',
                    'type_value' => $current_user->user_login,
                    'value' => $comment_id,
                    'rating_type' => 'comment' 
                ), array(
                     '%s',
                    '%s',
                    '%d',
                    '%s' 
                ) );
            }
        }
    }
    
    /**
     * Filter out comments order by top rated comment
	 *
     * @param array $comments
     */
    function ro_change_order( $comments ) {
        global $post;
        
        if ( !empty( $comments ) ) {
            $comment_id = $this->get_top_rated_comment( $post->ID );
            
            if ( $comment_id ) {
                foreach ( $comments as $key => $comment ) {
                    if ( $comment_id == $comment->comment_ID ) {
                        $top_array_key = $key;
                    }
                }
                
                $first_comment = $comments[0];
				
                $comments[$top_array_key]->comment_parent = 0;
                $comments[0] = $comments[$top_array_key];
                $comments[$top_array_key] = $first_comment;
            }
        }
        
        return apply_filters( 'elm_ur_comments_ro_change_order', $comments );
    }
    
    /**
     * Get top rated comment by comment post ID
	 *
     * @param int $comment_post_id
     */
    function get_top_rated_comment( $comment_post_id ) {
        global $wpdb;
		
		$comments_sql = $wpdb->get_results( $wpdb->prepare( "SELECT comment_id, rating_value, comment_type FROM {$wpdb->prefix}elm_ratings_comments WHERE post_id = %d ORDER By rating_value DESC", $comment_post_id ) );
		
		if ( $comments_sql ) {
			$comments = array();
			
			foreach ( $comments_sql as $key => $comment ) {
				if ( !isset( $comments[$comment->comment_id] ) )
					$comments[$comment->comment_id] = 0;
				
				if ( $comment->comment_type == 'like' ) {
					$comments[$comment->comment_id] += 1;
				} else {
					$comments[$comment->comment_id] -= 1;
				}
			}
			
			$comment_id = array_keys( $comments, max( $comments ) );
			
			return apply_filters( 'elm_ur_comments_get_top_rated_comment', $comment_id[0], $comment_post_id, $comments );
		}
    }
	
	/**
     * Get SVG image tag
	 *
	 * @param string $type
     * @param int $commentID
     */
	function get_svg_image( $type, $commentID ) {
		$filename_url = apply_filters( 'elm_ur_comments_svg_file', ELM_UR_PLUGIN_URL . '/svg/comments/' . $type . '.svg', $type, $commentID );
		
		if ( $type == 'like_one' ) {
			$img_tag = '<img src="'. $filename_url .'" alt="" class="elm-rate-comment like" />';
			
			// change type to like
			$type = 'like';
		} else if ( $type == 'like' ) {
			$img_tag = '<img src="'. $filename_url .'" alt="" class="elm-rate-comment like" />';
		} else if ( $type == 'dislike' ) { 
			$img_tag = '<img src="'. $filename_url .'" alt="" class="elm-rate-comment dislike" />';
		}
		
		if ( $this->get_comment_log_rating( $this->get_comment_log_method(), $commentID, $type ) ) {
			// unset class
			$img_tag = preg_replace( '/(<[^>]+) class=".*?"/i', '$1', $img_tag );
		}
		
		return apply_filters( 'elm_ur_comments_get_svg_img_tag', $img_tag, $type, $commentID );
	}
    
    /**
     * Template for comment rating form
	 *
     * @param int $commentID
     */
    function template( $commentID = 0 ) {
		global $post;
	
		if ( !$commentID )
			$commentID = get_comment_ID();
	
        $calculate_comment_rating = $this->calculate_comment_rating( $post->ID, $commentID );
		$calculate_comment_like = $this->calculate_comment_rating( $post->ID, $commentID, 'like' );
		$calculate_comment_dislike = $this->calculate_comment_rating( $post->ID, $commentID, 'dislike' );
		
        $comments_settings        = $this->get_settings->get_setting( 'comments' );
        
        if ( $calculate_comment_rating > 0 ) {
            $rating_total_css = 'green';
        } else if ( $calculate_comment_rating < 0 ) {
            $rating_total_css = 'red';
        } else {
            $rating_total_css = '';
        }
		
		do_action( 'elm_ur_comments_below_html_template', $calculate_comment_rating, $calculate_comment_like, $calculate_comment_dislike );
        
        $template_start = '<div class="comment-rating comment-' . $commentID . ' post-'. $post->ID .'">';
        
        $_template = $comments_settings['template'];
        
        $template_end = '</div>';
        
        // Replace shortcodes
        if ( $comments_settings['rating_image'] == 'image1' ) {
            $_template = str_replace( '%LIKE_SVG_ICON%', $this->get_svg_image( 'like_one', $commentID ), $_template );
            $_template = str_replace( '%DISLIKE_SVG_ICON%', '', $_template );
        }
        
        if ( $comments_settings['rating_image'] == 'image2' ) {
            $_template = str_replace( '%LIKE_SVG_ICON%', $this->get_svg_image( 'like', $commentID ), $_template );
            $_template = str_replace( '%DISLIKE_SVG_ICON%', $this->get_svg_image( 'dislike', $commentID ), $_template );
        }
        
        $_template = str_replace( '%COMMENT_RATING_CSS%', $rating_total_css, $_template );
        $_template = str_replace( '%COMMENT_TOTAL_RATING%', '<span class="comment-total-rating">' . $calculate_comment_rating . '</span>', $_template );
		$_template = str_replace( '%COMMENT_TOTAL_LIKE_RATING%', '<span class="comment-total-like-rating">' . $calculate_comment_like . '</span>', $_template );
		$_template = str_replace( '%COMMENT_TOTAL_DISLIKE_RATING%', '<span class="comment-total-dislike-rating">' . $calculate_comment_dislike . '</span>', $_template );
        
        // template
        $template = $template_start . $_template . $template_end;
		
		do_action( 'elm_ur_comments_after_html_template', $template, $calculate_comment_rating, $calculate_comment_like, $calculate_comment_dislike );
        
        return apply_filters( 'elm_ur_comments_html_template', $template, $calculate_comment_rating, $calculate_comment_like, $calculate_comment_dislike );
    }
    
    /**
     * Add comment rating form to comments
	 *
     * @param string $comment comment
     */
    function display_comment_rating( $comment ) {
		global $pagenow, $post;
		
		$post_types = $this->get_settings->get_setting( 'comments', 'allow_comment_rating_on' );
		
		if ( ! $post_types )
			$post_types = array();

        if ( $post ) {
            // Check if we allow comment rating for the current post type
            if (!array_key_exists(get_post_type($post), $post_types))
                return $comment;

            // Compatibility with WooCommerce reviews
            if (get_post_type($post) == 'product' && get_option('woocommerce_enable_review_rating') == 'yes') {
                return $comment;
            }
        } else {
            return $comment;
        }
		
		if ( @$pagenow == 'edit-comments.php' )
			return $comment;
	
        $template = $this->template( );
		
        return $comment . $template;
    }
    
    /**
     * Add comment rating to the database
	 *
     * @param int $comment_id
     * @param string $type
     */
    function add_comment_rating( $comment_id, $post_id, $type ) {
        global $wpdb;
		
		$count = 0;
		
		if ( $type == 'like' ) {
			$count = 1;
		} else {
			$count -= 1;
		}
		
		$wpdb->insert( $wpdb->prefix . 'elm_ratings_comments', array(
			'post_id' => $post_id,
			'comment_id' => $comment_id,
			'rating_value' => $count,
			'comment_type' => $type
		));
		
		do_action( 'elm_ur_comments_add_comment_rating', $comment_id, $post_id, $type );
    }
    
    /**
     * Calculate comment rating
	 *
     * @param int $comment_id
     */
    function calculate_comment_rating( $post_id, $comment_id, $total = 'total' ) {
        global $wpdb;
		
		$average = 0;
        
		if ( $total == 'total' ) {
			$likes = 0;
			$dislikes = 0;
		
			$likes = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(rating_value) FROM {$wpdb->prefix}elm_ratings_comments WHERE post_id = %d AND comment_id = %d AND comment_type = %s", $post_id, $comment_id, 'like' ) );
			
			$dislikes = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(rating_value) FROM {$wpdb->prefix}elm_ratings_comments WHERE post_id = %d AND comment_id = %d AND comment_type = %s", $post_id, $comment_id, 'dislike' ) );
			
			$average = $likes - $dislikes;
		} else if ( $total == 'like' ) {
			$average = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(rating_value) FROM {$wpdb->prefix}elm_ratings_comments WHERE post_id = %d AND comment_id = %d AND comment_type = %s", $post_id, $comment_id, 'like' ) );
		
		} else if ( $total == 'dislike' ) {
			$average = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(rating_value) FROM {$wpdb->prefix}elm_ratings_comments WHERE post_id = %d AND comment_id = %d AND comment_type = %s", $post_id, $comment_id, 'dislike' ) );
			
			$average = 0 - $average;
		}
        
		return apply_filters( 'elm_ur_comments_calculate_comment_average', $average, $post_id, $comment_id, $total );
    }
}

endif;